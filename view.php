<?php

include '_config.php';
include '_functions.php';
include '_global.php';

$overall_count = 0;
$overall_api = 0;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crawl Results</title>
    <link rel="stylesheet" type="text/css" href="css/phd.css">

</head>
<body>
<?php menu();?>
<div class="graphs">
<?php

$sql = "SELECT table_name FROM information_schema.tables where table_schema='viewports' order by table_name ASC;";

$result = mysqli_query($con, $sql);

if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {

        $human_name = str_replace('-university', '', $row['table_name']);
        $human_name = str_replace('-', ' ', $human_name);
        $human_name = ucwords($human_name);

        do_graph($row['table_name'], '<strong>' . $human_name . '</strong>', '', 'show-number');

    }
}

?>
</div>
<div class="overall-stats">
<p><?php echo $overall_count; ?> websites investigated. <?php echo $overall_api; ?> records found in the internet archive: average <?php echo round($overall_api / $overall_count); ?> per site.</p>
</div>
</body>
</html>
