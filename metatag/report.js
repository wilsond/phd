var intWidth = window.innerWidth;
var outWidth = window.outerWidth;

var printout = 'A typical sentence of content: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras orci sem, vulputate luctus mi at, ultricies semper augue. Nulla at risus in neque laoreet pellentesque. Phasellus sed metus pharetra, mollis magna a, condimentum ex. Donec in ullamcorper turpis. <b>The screen size reported is ' + intWidth + 'px (' + outWidth + 'px)</b>';

var show = document.querySelector('p');

show.innerHTML = printout;