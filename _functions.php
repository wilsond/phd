<?php

// slug items

function slug($content)
{
    return strtolower(str_replace(" ", "-", trim($content)));
}

// clean up name

function table_cleanup($name)
{
    $name = str_replace('-university', '', $name);
    $name = str_replace('-', ' ', $name);
    return ucwords($name);

}

// get a CSV list

function getlist($file)
{

    $listing = array();

    if (($handle = fopen($file, "r")) !== false) {
        while (($data = fgetcsv($handle, 1000, ",")) !== false) {

            $alist['name'] = $data[2];
            $alist['type'] = $data[0];
            $alist['country'] = $data[1];
            $alist['url'] = $data[3];
            array_push($listing, $alist);

        }
    }
    return $listing;
}

// do a CURL request to get the data

function get_data($url)
{

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
    //curl_setopt($ch, CURLOPT_TIMEOUT, 400);

    $data = curl_exec($ch);

    curl_close($ch);

    $doc = new DOMDocument();
    @$doc->loadHTML($data);

    $metas = $doc->getElementsByTagName('meta');

    echo '<p class="meta">Meta <strong>' . $metas->length . '</strong></p>';
    $found_meta = '';
    for ($i = 0; $i < $metas->length; $i++) {

        $meta = $metas->item($i);
//echo $meta->getAttribute('name').' '.$meta->getAttribute('content');

// some are doing this twice?
        if ($meta->getAttribute('name') == "viewport") {
            $found_meta = $meta->getAttribute('content');
            echo '<p class="good-meta"><strong>&#10004; viewport</strong></p>';
        }

    }

    return $found_meta;
}

function find_viewport($url, $id, $start_year, $finish_year, $con, $allowed_distance, $total_count, $count)
{

    global $database_table;

    $test_urls = array();

    $check_file = 'http://web.archive.org/cdx/search/cdx?url=' . $url;

    
    $list = @file($check_file);

    
    if (count($list) > 1) {

        echo '<div class="site-check"><h2><span class="counter">' . $count . ' of ' . $total_count . '</span> Testing site at <strong>' . $url . '</strong> &middot; ' . count($list) . ' crawls found</h2>';

        $sql = "UPDATE `$database_table` SET `api_crawls` = " . count($list) . " WHERE `$database_table`.`id` = " . $id . ";";
        $result = mysqli_query($con, $sql);

        $current_year = '';
// clean the list so it's only 'proper' crawls
        $clean_list = array();
        foreach ($list as $l) {
            if (strstr($l, 'warc/revisit')) {array_push($clean_list, $l);} // this for universities that have changed name
            if (strstr($l, 'text/html')) {array_push($clean_list, $l);} // this to be preferred

        }

        foreach ($clean_list as $l) {
            // comment this back in to see what's coming out from the API
            //echo $l.'<br />';
            $l = explode(' ', $l);

            $test_date = substr($l[1], 0, 4);
            if ($test_date !== $current_year) {
                $test_urls[$test_date] = $l[1];
                $current_year = $test_date;
            }
        }

        for ($i = $start_year; $i <= $finish_year; $i++) {

            if (isset($test_urls[$i]) && $test_urls[$i] !== "") {
                echo '<div class="check-year"><h3>' . $i . '</h3>';
                $code = get_data('http://web.archive.org/web/' . $test_urls[$i] . 'id_/' . $url);
                echo '</div>';

                // if($code!==""){echo '<p class="good">Media query found</p>';}else{echo '<p class="bad">No media query found</p>';}

                $sql = "UPDATE `$database_table` SET `v_" . $i . "` = '" . $code . "' WHERE `$database_table`.`id` = " . $id . ";";
//echo "<p>SQL query: ".$sql.'</p>';
            } else {
                echo '<div class="check-year bad"><h3>' . $i . '</h3><p>Not in API</p></div>';
                $sql = "UPDATE `$database_table` SET `v_" . $i . "` = 'no api' WHERE `$database_table`.`id` = " . $id . ";";
//echo "<p>SQL query: ".$sql.'</p>';

            }
            $result = mysqli_query($con, $sql);

            flush();

        }
        // university complete, update last_collected time
        $sql = "UPDATE `$database_table` SET `last_collected` = now() WHERE `$database_table`.`id` = " . $id . ";";
        $result = mysqli_query($con, $sql);
    } else {
        $why = get_headers($check_file);
        echo '<div class="site-check"><h2><span class="counter">' . $count . ' of ' . $total_count . '</span> Testing site at <strong>' . $url . '</strong> &middot; no crawls found in API</h2><p><span class="error">' . $check_file . '</span></p><p><span class="error">' . $why[5] . '</span></p></div>';

    }
}

function find_live_viewport($url, $id, $con)
{

    global $database_table;
    $code = @get_data($url);

    $sql = "UPDATE `$database_table` SET `v_now` = '" . $code . "' WHERE `$database_table`.`id` = " . $id . ";";
    echo '<p>' . $sql . '</p>';
    $result = mysqli_query($con, $sql);
    flush();

}

// create a graph

function do_graph($table, $heading, $extra_sql, $show_number)
{

    $database_table = $table;
    global $overall_count;
    global $overall_api;
    global $start_year;
    global $finish_year;
    global $con;

    $sql = "SELECT count(*) FROM `$database_table`;";
    if (isset($extra_sql) && $extra_sql !== "") {

        $sql = 'SELECT count(*) FROM `' . $database_table . '` WHERE `id` IN (' . implode(',', array_map('intval', $extra_sql)) . ');';
    }

    if ($result = mysqli_query($con, $sql)) {
        while ($row = mysqli_fetch_array($result)) {
            $total = $row[0];
        }
    }

    echo '<div class="a-graph">';
    echo '<h1>' . $heading . '</h1>';
    if ($total > 0) {
// if(isset($extra_sql)&&$extra_sql!==""){

//     $sql='SELECT v_'.$i.', count('.$i.') FROM `'.$database_table.'` where v_'.$i.' <> \'\' and `id` IN (' . implode(',', array_map('intval', $extra_sql)) . ');';
        //     }
        // $sql="SELECT v_now, count(v_now) FROM `$database_table` where v_now <> '';";

// if(isset($extra_sql)&&$extra_sql!==""){
        // $sql='SELECT v_now, count(v_now) FROM `'.$database_table.'` where v_now <> \'\' and `id` IN (' . implode(',', array_map('intval', $extra_sql)) . ');';
        // }

        echo '<ul class="graph">';

        for ($i = $start_year; $i <= $finish_year; $i++) {
            echo '<li><span class="year">' . $i . '</span><span class="bars">';
// all MQs
            $sql = "SELECT v_$i, count(v_$i) FROM `$database_table` where last_collected <> '0000-00-00 00:00:00' AND v_$i <> '' AND v_$i<>'no api';";

            if ($result = mysqli_query($con, $sql)) {
                while ($row = mysqli_fetch_array($result)) {

                    $didfind = round(100 * ($row['count(v_' . $i . ')'] / $total), 0);

                    echo '<span class="bar media" style="width:' . (100 * ($row['count(v_' . $i . ')'] / $total)) . '%" title="' . $row['count(v_' . $i . ')'] . '"></span>';

//     echo '<li><span class="number">'.$i.'</span>';

//     $pixel_width = $row['count(v_now)'] * 4;

//     echo '<span class="bar" style="width:'.($stat*5).'px;">'.$row['count(v_now)'].'</span><span class="percent">'.round($stat).'%</span></li>';

                }
            }

// all rejects
            $sql = "SELECT v_$i, count(v_$i) FROM `$database_table` where last_collected <> '0000-00-00 00:00:00' AND v_$i = 'no api';";

            if ($result = mysqli_query($con, $sql)) {
                while ($row = mysqli_fetch_array($result)) {

                    echo '<span class="bar reject" style="width:' . (100 * ($row['count(v_' . $i . ')'] / $total)) . '%"></span>';

                }
            }
// all not found
            $sql = "SELECT v_$i, count(v_$i) FROM `$database_table` where last_collected <> '0000-00-00 00:00:00' AND v_$i = '';";
            if ($result = mysqli_query($con, $sql)) {
                while ($row = mysqli_fetch_array($result)) {

                    echo '<span class="bar not-found" style="width:' . (100 * ($row['count(v_' . $i . ')'] / $total)) . '%"></span>';

                }
            }

            echo '</span><span class="percentage">' . $didfind . '%</span></li>';

        }
        $api_total = 0;
        $sql = "select sum(api_crawls) AS total FROM `$database_table`;";
        if ($result = mysqli_query($con, $sql)) {
            while ($row = mysqli_fetch_array($result)) {
                $api_total = $row['total'];
            }
        }

        echo '</ul>';

        if ($show_number == "show-number") {
            echo '<p>' . $total . ' websites investigated<br />' . $api_total . ' API records found &middot; ' . round($api_total / $total) . ' per website</p>';
        }

        $overall_api = $overall_api + $api_total;
        $overall_count = $overall_count + $total;
    } else {echo '<p class="error">No websites found!</p>';}
    echo '</div>';
}

function do_analysis($countries, $total_sites)
{
    //print_r($countries);
    global $start_year;
    global $finish_year;
    global $con;
    echo '<ul class="graph no-width">';
    // start with a year

    for ($i = $start_year; $i <= $finish_year; $i++) {

        //$year_count = 0;
        $year_examples = array();
        $country_data = array();

        foreach ($countries as $c) {
            $database_table = $c;
            $j = 0;

            $sql = "SELECT v_$i FROM `$database_table` where last_collected <> '0000-00-00 00:00:00' AND v_$i <> '' AND v_$i<>'no api';";
            //echo $sql;
            if ($result = mysqli_query($con, $sql)) {
                while ($row = mysqli_fetch_array($result)) {
                    //print_r($row);
                    array_push($year_examples, $row);
                    $country_data[$database_table][$j] = $row;
                    //$year_count = $year_count + $row['count(v_' . $i . ')'];
                    $j++;
                }
            }

        }
        //print_r($year_examples);
        //print_r($country_data);

        $examples = new RecursiveIteratorIterator(new RecursiveArrayIterator($year_examples));
        $find_types = array();
        foreach ($examples as $e) {
            array_push($find_types, str_replace(" ", "", $e));
        }

        //print_r($find_types);

        echo '<li>' . count($find_types) . ' websites</li>';

        $magnification = 6;

        $numeric_width = array();
        preg_match_all("/width=[0-9]/", implode('', $find_types), $numeric_width);

        $dw_width = array();
        preg_match_all("/width=device-width/", implode('', $find_types), $dw_width);

        $bad_max = array();
        preg_match_all("/maximum-scale=1/", implode('', $find_types), $bad_max);
        $pinch = array();
        preg_match_all("/user-scalable=no/", implode('', $find_types), $pinch);

        echo '<li><span class="year">' . $i . '</span><span class="bars">';
        echo '<span class="bar media" style="width:' . count($year_examples) / $magnification . 'px"></span><span class="percentage">' . 100 * round(count($year_examples) / $total_sites, 4) . '%</span>';
        echo '</li>';

        echo '<li><span class="year"></span><span class="bars">';
        echo '<span class="bar number" style="width:' . count($numeric_width[0]) / $magnification . 'px"></span><span class="percentage">width=[number] ' . 100 * round(count($numeric_width[0]) / $total_sites, 4) . '%</span>';
        echo '</li>';

        echo '<li><span class="year"></span><span class="bars">';
        echo '<span class="bar wdw" style="width:' . count($dw_width[0]) / $magnification . 'px"></span><span class="percentage">width=device-width ' . 100 * round(count($dw_width[0]) / $total_sites, 4) . '%</span>';
        echo '</li>';

        echo '<li><span class="year"></span><span class="bars">';
        echo '<span class="bar badmax" style="width:' . count($bad_max[0]) / $magnification . 'px"></span><span class="percentage">max-scale=1 ' . 100 * round(count($bad_max[0]) / $total_sites, 4) . '%</span>';
        echo '</li>';

        echo '<li><span class="year"></span><span class="bars">';
        echo '<span class="bar pinch" style="width:' . count($pinch[0]) / $magnification . 'px"></span><span class="percentage">user-scalable=no ' . 100 * round(count($pinch[0]) / $total_sites, 4) . '%</span>';
        echo '</li>';

        echo "<li class=\"space\"></li>";

        echo "<li class=\"country-data\"><table><tr><th>country</th><th>number width</th><th>device width</th><th>bad max scale</th><th>bad user scalable</th></tr>";

        ksort($country_data);

        foreach ($country_data as $name => $c_data) {
            echo '<tr><td>' . table_cleanup($name) . '</td>';

            $examples = new RecursiveIteratorIterator(new RecursiveArrayIterator($c_data));
            $data = array();
            foreach ($examples as $e) {
                array_push($data, str_replace(" ", "", $e));
            }
// for some reason the data's being duplicated

            $c_numeric_width = array();
            preg_match_all("/width=[0-9]/", implode('', $data), $c_numeric_width);

            echo '<td>' . count($c_numeric_width[0]) / 2 . '<span class="less">/' . count($country_data[$name]) . '</span></td>';

            $c_dw_width = array();
            preg_match_all("/width=device-width/", implode('', $data), $c_dw_width);

            echo '<td>' . count($c_dw_width[0]) / 2 . '<span class="less">/' . count($country_data[$name]) . '</span></td>';

            $c_bad_max = array();
            preg_match_all("/maximum-scale=1/", implode('', $data), $c_bad_max);

            echo '<td>' . count($c_bad_max[0]) / 2 . '<span class="less">/' . count($country_data[$name]) . '</span></td>';

            $c_pinch = array();
            preg_match_all("/user-scalable=no/", implode('', $data), $c_pinch);
            echo '<td>' . count($c_pinch[0]) / 2 . '<span class="less">/' . count($country_data[$name]) . '</span></td>';

            echo '</tr>';

        }

        echo "</table></li>";
    }

// examine the array

// do it again
    echo '</ul>';
}

function do_analysis_no_table($countries, $total_sites)
{
    //print_r($countries);
    global $start_year;
    global $finish_year;
    global $con;
    echo '<ul class="main-graph graph no-width">';
    // start with a year

    for ($i = $start_year; $i <= $finish_year; $i++) {

        //$year_count = 0;
        $year_examples = array();
        $country_data = array();

        foreach ($countries as $c) {
            $database_table = $c;
            $j = 0;

            $sql = "SELECT v_$i FROM `$database_table` where last_collected <> '0000-00-00 00:00:00' AND v_$i <> '' AND v_$i<>'no api';";
            //echo $sql;
            if ($result = mysqli_query($con, $sql)) {
                while ($row = mysqli_fetch_array($result)) {
                    //print_r($row);
                    array_push($year_examples, $row);
                    $country_data[$database_table][$j] = $row;
                    //$year_count = $year_count + $row['count(v_' . $i . ')'];
                    $j++;
                }
            }

        }
        //print_r($year_examples);
        //print_r($country_data);

        $examples = new RecursiveIteratorIterator(new RecursiveArrayIterator($year_examples));
        $find_types = array();
        foreach ($examples as $e) {
            array_push($find_types, str_replace(" ", "", $e));
        }

        //print_r($find_types);

        $magnification = 6;

        $numeric_width = array();
        preg_match_all("/width=[0-9]/", implode('', $find_types), $numeric_width);

        $dw_width = array();
        preg_match_all("/width=device-width/", implode('', $find_types), $dw_width);

        $bad_max = array();
        preg_match_all("/maximum-scale=1/", implode('', $find_types), $bad_max);
        $pinch = array();
        preg_match_all("/user-scalable=no/", implode('', $find_types), $pinch);

        echo '<li class="year-bar"><span class="year"><strong>' . $i . '</strong></span></li>';
        echo '<li><span class="year"><strong>' . count($find_types) . ' RWD websites</strong></span><span class="bars">';
        echo '<span class="bar media" style="width:' . count($year_examples) / $magnification . 'px"></span><span class="percentage">' . 100 * round(count($year_examples) / $total_sites, 4) . '% of sample</span>';
        echo '</li>';

        echo '<li><span class="year">width=[number]</span><span class="bars">';

        // this is a percentage so it should stay constant

        echo '<span class="bar number topercent" style="width:' . $magnification * (100 * round(count($numeric_width[0]) / count($find_types), 4)) . 'px"></span><span class="percentage">' . 100 * round(count($numeric_width[0]) / count($find_types), 4) . '% &middot; ' . count($numeric_width[0]) . '  websites</span>';

        echo '</li>';

        echo '<li><span class="year">width=device-width</span><span class="bars">';
        echo '<span class="bar wdw topercent" style="width:' . $magnification * (100 * round(count($dw_width[0]) / count($find_types), 4)) . 'px"></span><span class="percentage">' . 100 * round(count($dw_width[0]) / count($find_types), 4) . '% &middot; ' . count($dw_width[0]) . ' websites</span>';
        echo '</li>';

        echo '<li><span class="year">max-scale=1</span><span class="bars">';
        echo '<span class="bar topercent badmax" style="width:' . $magnification * (100 * round(count($bad_max[0]) / count($find_types), 4)) . 'px"></span><span class="percentage">' . 100 * round(count($bad_max[0]) / count($find_types), 4) . '% &middot; ' . count($bad_max[0]) . ' websites</span>';
        echo '</li>';

        echo '<li><span class="year">user-scalable=no</span><span class="bars">';
        echo '<span class="bar topercent pinch" style="width:' . $magnification * (100 * round(count($pinch[0]) / count($find_types), 4)) . 'px"></span><span class="percentage">' . 100 * round(count($pinch[0]) / count($find_types), 4) . '% &middot; ' . count($pinch[0]) . ' websites</span>';
        echo '</li>';

//         echo "<li class=\"space\"></li>";

//         echo "<li class=\"country-data\"><table><tr><th>country</th><th>number width</th><th>device width</th><th>bad max scale</th><th>bad user scalable</th></tr>";

//         ksort($country_data);

//         foreach ($country_data as $name => $c_data) {
        //             echo '<tr><td>' . table_cleanup($name) . '</td>';

//             $examples = new RecursiveIteratorIterator(new RecursiveArrayIterator($c_data));
        //             $data = array();
        //             foreach ($examples as $e) {
        //                 array_push($data, str_replace(" ", "", $e));
        //             }
        // // for some reason the data's being duplicated

//             $c_numeric_width = array();
        //             preg_match_all("/width=[0-9]/", implode('', $data), $c_numeric_width);

//             echo '<td>' . count($c_numeric_width[0]) / 2 . '<span class="less">/' . count($country_data[$name]) . '</span></td>';

//             $c_dw_width = array();
        //             preg_match_all("/width=device-width/", implode('', $data), $c_dw_width);

//             echo '<td>' . count($c_dw_width[0]) / 2 . '<span class="less">/' . count($country_data[$name]) . '</span></td>';

//             $c_bad_max = array();
        //             preg_match_all("/maximum-scale=1/", implode('', $data), $c_bad_max);

//             echo '<td>' . count($c_bad_max[0]) / 2 . '<span class="less">/' . count($country_data[$name]) . '</span></td>';

//             $c_pinch = array();
        //             preg_match_all("/user-scalable=no/", implode('', $data), $c_pinch);
        //             echo '<td>' . count($c_pinch[0]) / 2 . '<span class="less">/' . count($country_data[$name]) . '</span></td>';

//             echo '</tr>';

//         }

//         echo "</table></li>";
    }

// examine the array

// do it again
    echo '</ul>';
}

function do_analysis_output_csv($countries, $total_sites)
{
    //print_r($countries);
    global $start_year;
    global $finish_year;
    global $con;

    // start with a year

    for ($i = $start_year; $i <= $finish_year; $i++) {

        //$year_count = 0;
        $year_examples = array();
        $country_data = array();

        foreach ($countries as $c) {
            $database_table = $c;
            $j = 0;

            $sql = "SELECT v_$i FROM `$database_table` where last_collected <> '0000-00-00 00:00:00' AND v_$i <> '' AND v_$i<>'no api';";
            //echo $sql;
            if ($result = mysqli_query($con, $sql)) {
                while ($row = mysqli_fetch_array($result)) {
                    //print_r($row);
                    array_push($year_examples, $row);
                    $country_data[$database_table][$j] = $row;
                    //$year_count = $year_count + $row['count(v_' . $i . ')'];
                    $j++;
                }
            }

        }
        //print_r($year_examples);
        //print_r($country_data);

        $examples = new RecursiveIteratorIterator(new RecursiveArrayIterator($year_examples));
        $find_types = array();
        foreach ($examples as $e) {
            array_push($find_types, str_replace(" ", "", $e));
        }

        //print_r($find_types);

        $magnification = 6;

        $numeric_width = array();
        preg_match_all("/width=[0-9]/", implode('', $find_types), $numeric_width);

        $dw_width = array();
        preg_match_all("/width=device-width/", implode('', $find_types), $dw_width);

        $bad_max = array();
        preg_match_all("/maximum-scale=1/", implode('', $find_types), $bad_max);
        $pinch = array();
        preg_match_all("/user-scalable=no/", implode('', $find_types), $pinch);

        echo $i . ',';
        echo count($find_types) . ',';
        echo 100 * round(count($year_examples) / $total_sites, 4) . '%,';

        // this is a percentage so it should stay constant

        echo 100 * round(count($numeric_width[0]) / count($find_types), 4) . '%,' . count($numeric_width[0]) . ',';

        echo 100 * round(count($dw_width[0]) / count($find_types), 4) . '%,' . count($dw_width[0]) . ',';

        echo 100 * round(count($bad_max[0]) / count($find_types), 4) . '%,' . count($bad_max[0]) . ',';

        echo 100 * round(count($pinch[0]) / count($find_types), 4) . '%,' . count($pinch[0]) . "\r\n";

//         echo "<li class=\"space\"></li>";

//         echo "<li class=\"country-data\"><table><tr><th>country</th><th>number width</th><th>device width</th><th>bad max scale</th><th>bad user scalable</th></tr>";

//         ksort($country_data);

//         foreach ($country_data as $name => $c_data) {
        //             echo '<tr><td>' . table_cleanup($name) . '</td>';

//             $examples = new RecursiveIteratorIterator(new RecursiveArrayIterator($c_data));
        //             $data = array();
        //             foreach ($examples as $e) {
        //                 array_push($data, str_replace(" ", "", $e));
        //             }
        // // for some reason the data's being duplicated

//             $c_numeric_width = array();
        //             preg_match_all("/width=[0-9]/", implode('', $data), $c_numeric_width);

//             echo '<td>' . count($c_numeric_width[0]) / 2 . '<span class="less">/' . count($country_data[$name]) . '</span></td>';

//             $c_dw_width = array();
        //             preg_match_all("/width=device-width/", implode('', $data), $c_dw_width);

//             echo '<td>' . count($c_dw_width[0]) / 2 . '<span class="less">/' . count($country_data[$name]) . '</span></td>';

//             $c_bad_max = array();
        //             preg_match_all("/maximum-scale=1/", implode('', $data), $c_bad_max);

//             echo '<td>' . count($c_bad_max[0]) / 2 . '<span class="less">/' . count($country_data[$name]) . '</span></td>';

//             $c_pinch = array();
        //             preg_match_all("/user-scalable=no/", implode('', $data), $c_pinch);
        //             echo '<td>' . count($c_pinch[0]) / 2 . '<span class="less">/' . count($country_data[$name]) . '</span></td>';

//             echo '</tr>';

//         }

//         echo "</table></li>";
    }

// examine the array

// do it again

}

// graphs for interesting things by country
// find any with width = and compare with width = device width

function do_width_graph($table, $heading)
{

    $database_table = $table;
    global $overall_count;
    global $overall_api;
    global $start_year;
    global $finish_year;
    global $con;

    $all_widths = array();

    $sql = "SELECT count(*) FROM `$database_table`;";

    if ($result = mysqli_query($con, $sql)) {
        while ($row = mysqli_fetch_array($result)) {
            $total = $row[0];
        }
    }

    echo '<div class="a-graph" style="min-height: 650px;">';
    echo '<h1>' . $heading . ' · width=[number] vs width=device-width</h1>';
    if ($total > 0) {

        echo '<ul class="graph">';

        for ($i = $start_year; $i <= $finish_year; $i++) {

            $year_examples = array();

            echo '<li><span class="year">' . $i . '</span><span class="bars">';

            $count_sql = "SELECT count(v_$i) FROM `$database_table` where last_collected <> '0000-00-00 00:00:00' AND v_$i <> '' AND v_$i<>'no api';";

            if ($result = mysqli_query($con, $count_sql)) {

                while ($row = mysqli_fetch_array($result)) {

                    echo '<span class="r-o"><strong>' . $row['count(v_' . $i . ')'] . '</strong></span>';
                    $total_possible = $row['count(v_' . $i . ')'];
                }

            }
            $full_sql = "SELECT v_$i FROM `$database_table` where last_collected <> '0000-00-00 00:00:00' AND v_$i <> '' AND v_$i<>'no api';";

            if ($result = mysqli_query($con, $full_sql)) {

                while ($row = mysqli_fetch_array($result)) {
                    array_push($year_examples, $row['v_' . $i]);

                }

                //print_r($year_examples);
                //print_r($country_data);

                $examples = new RecursiveIteratorIterator(new RecursiveArrayIterator($year_examples));
                $find_types = array();
                foreach ($examples as $e) {
                    array_push($find_types, str_replace(" ", "", $e));
                }

                //print_r($find_types);

                $numeric_width = array();
                preg_match_all("/width=\d\d*/", implode('', $find_types), $numeric_width);
                //print_r($numeric_width);
                echo '<span class="r-o"><code>number-width</code> ' . count($numeric_width[0]) . '</span>';
                array_push($all_widths, $numeric_width);
                $dw_width = array();
                preg_match_all("/width=device-width/", implode('', $find_types), $dw_width);
                echo '<span class="r-o"><code>device-width</code> ' . count($dw_width[0]) . '</span>';

            }

            echo '</li>';

            $total_width = count($numeric_width[0]) + count($dw_width[0]);

            if ($total_width == 0) {
                echo '<li><span class="year">&nbsp;</span><span class="bars assume100">&nbsp;</span></li>';
            } else {

                echo '<li><span class="year">&nbsp;</span><span class="bars assume100"><span class="bar numberw" style="width:' . (count($numeric_width[0]) / $total_width) * 100 . '%"></span><span class="bar numberdw" style="width:' . (count($dw_width[0]) / $total_width) * 100 . '%"></span></span></li>';
            }

        }

        echo '</ul>';
    } else {echo '<p class="error">No websites found!</p>';}

    echo '</div>';

    return $all_widths;
}

// create a graph
// find any with maximum-scale = 1 or user-scalable = no

function do_bad_graph($table, $heading)
{

    $database_table = $table;
    global $overall_count;
    global $overall_api;
    global $start_year;
    global $finish_year;
    global $con;

    $sql = "SELECT count(*) FROM `$database_table`;";
    if (isset($extra_sql) && $extra_sql !== "") {

        $sql = 'SELECT count(*) FROM `' . $database_table . '` WHERE `id` IN (' . implode(',', array_map('intval', $extra_sql)) . ');';
    }

    if ($result = mysqli_query($con, $sql)) {
        while ($row = mysqli_fetch_array($result)) {
            $total = $row[0];
        }
    }

    echo '<div class="a-graph" style="min-height: 650px;">';
    echo '<h1>' . $heading . ' · max-scale=1 or user-scalable=no</h1>';
    if ($total > 0) {

        echo '<ul class="graph">';

        for ($i = $start_year; $i <= $finish_year; $i++) {

            $year_examples = array();

            echo '<li><span class="year">' . $i . '</span><span class="bars">';

            $count_sql = "SELECT count(v_$i) FROM `$database_table` where last_collected <> '0000-00-00 00:00:00' AND v_$i <> '' AND v_$i<>'no api';";

            if ($result = mysqli_query($con, $count_sql)) {

                while ($row = mysqli_fetch_array($result)) {

                    echo '<span class="r-o"><strong>' . $row['count(v_' . $i . ')'] . '</strong></span>';
                    $total_possible = $row['count(v_' . $i . ')'];
                }

            }
            $full_sql = "SELECT v_$i FROM `$database_table` where last_collected <> '0000-00-00 00:00:00' AND v_$i <> '' AND v_$i<>'no api';";

            if ($result = mysqli_query($con, $full_sql)) {
                //$j = 0;
                while ($row = mysqli_fetch_array($result)) {
                    //print_r($row);
                    array_push($year_examples, $row['v_' . $i]);
                    //$country_data[$database_table][$j] = $row;
                    //$year_count = $year_count + $row['count(v_' . $i . ')'];
                    //$j++;
                }

                //print_r($year_examples);
                //print_r($country_data);

                $examples = new RecursiveIteratorIterator(new RecursiveArrayIterator($year_examples));
                $find_types = array();
                foreach ($examples as $e) {
                    array_push($find_types, str_replace(" ", "", $e));
                }

                //print_r($find_types);

                $total_bad = 0;

                $bad_max = array();
                preg_match_all("/maximum-scale=1/", implode('', $find_types), $bad_max);
                echo '<span class="r-o"><code>maxscale=1</code> ' . count($bad_max[0]) . '</span>';
                $total_bad = $total_bad + count($bad_max[0]);
                $pinch = array();
                preg_match_all("/user-scalable=no/", implode('', $find_types), $pinch);
                echo '<span class="r-o"><code>user-scalable=no</code> ' . count($pinch[0]) . '</span>';
                $total_bad = $total_bad + count($pinch[0]);
            }
            echo "</li>";

            if ($total_bad > 0) {

                echo '<li><span class="year">&nbsp;</span><span class="bars assume100"><span class="bar bad" style="width:' . ($total_bad / $total_possible) * 100 . '%"></span></span></li>';
            } else {
                echo '<li><span class="year">&nbsp;</span><span class="bars assume100">&nbsp;</span></li>';
            }
        }

        echo '</ul>';
    } else {echo '<p class="error">No websites found!</p>';}

    echo '</div><br clear="all" />';
}



function do_country_analysis_output_csv($countries, $total_sites)
{
    //print_r($countries);
    global $start_year;
    global $finish_year;
    global $con;

    // start with a year



foreach($countries as $c){
    $database_table = $c;
    
    $initial_sql = "SELECT COUNT(*) FROM `$database_table`;";
    
    if ($result = mysqli_query($con, $initial_sql)) {
        while ($row = mysqli_fetch_array($result)) {
            $country_total = $row[0];
        }
    }
    

            $j = 0;
            $current_country=array();
            for ($i = $start_year; $i <= $finish_year; $i++) {
            $sql = "SELECT count(v_$i) FROM `$database_table` where last_collected <> '0000-00-00 00:00:00' AND v_$i <> '' AND v_$i<>'no api';";
            //echo $sql;
            if ($result = mysqli_query($con, $sql)) {
                while ($row = mysqli_fetch_array($result)) {
                    //print_r($row);
                    array_push($current_country, $row);
                    
                    
                }
            }
            
          
            
        }
        //print_r($current_country);
        if($country_total>0){
        echo "$c";
    
        foreach($current_country as $c_num){
            $year_total = $c_num[0];
            $total_sites =  $country_total;
            
            echo ",".round(($year_total/$total_sites)*100,2);
        }
    echo "\r\n";
        }
}

}

function breakdown($university){
    
    global $start_year;
    global $finish_year;
    global $con;
    
    for($i=$start_year; $i<=$finish_year; $i++){
        $no_api_results = array();
        $viewport_results = array();
    echo '<div class="year"><h2>'.$i.'</h2>';
   $sql =  "SELECT site_name, v_$i FROM `$university` WHERE `v_$i` <> '' ORDER BY site_name asc;";
   if ($result = mysqli_query($con, $sql)) {
    while ($row = mysqli_fetch_array($result)) {
        if($row[1]=='no api'){
            array_push($no_api_results, $row[0]);
        }else{
            array_push($viewport_results, $row[0]);
        }
    }
    echo '<h3>Viewports found: '.count($viewport_results).'</h3><ul class="viewport-found">';
    foreach($viewport_results as $v){
     echo "<li>$v</li>";   
    }
    
    echo '</ul><h3>Not in Internet Archive</h3><ul class="not-archived">';
    foreach($no_api_results as $n){
        echo "<li>$n</li>";   
       }
  echo '</ul>';
    
    
}
echo '</div>';
    }
    
}