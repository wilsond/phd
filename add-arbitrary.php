<?php
include '_global.php';
include '_config.php';
include '_functions.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add an arbitrary list</title>
    <link rel="stylesheet" href="css/phd.css" />
</head>
<body>
<?php menu();?>
<div class="add-uni">

<?php

function get_http_response_code($domain1)
{
    $headers = get_headers($domain1);
    return substr($headers[0], 9, 3);
}

$c = 'russell';
$c = slug($c);

$make_table = 'CREATE TABLE `' . $c . '-university` (
    `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `site_name` text NOT NULL,
    `site_url` text NOT NULL,
    `initial_add` timestamp NOT NULL,
    `api_crawls` int NOT NULL,
    `v_2007` text NOT NULL,
    `v_2008` text NOT NULL,
    `v_2009` text NOT NULL,
    `v_2010` text NOT NULL,
    `v_2011` text NOT NULL,
    `v_2012` text NOT NULL,
    `v_2013` text NOT NULL,
    `v_2014` text NOT NULL,
    `v_2015` text NOT NULL,
    `v_2016` text NOT NULL,
    `v_2017` text NOT NULL,
    `last_collected` timestamp NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;';

//echo $make_table;

$result_start = mysqli_query($con, 'DROP TABLE IF EXISTS `' . $c . '-university`;');

$result = mysqli_query($con, $make_table);

$list = file('arbitrary-lists/russell.csv');

//print_r($list);

//var_dump($list);
echo '<h1 class="uni-list-header">' . count($list) . ' sites to add to database table for ' . $c . '</h1>';

echo '<ul class="uni-list">';
foreach ($list as $obj) {

    $obj = explode(',', $obj);

    $url = trim($obj[0]);

    echo '<li><a href="' . $url . '">' . $obj[1] . '</a></li>';

    //echo get_http_response_code($url);

    $sql = 'INSERT INTO `' . $c . '-university` (site_name, site_url, initial_add) values (\'' . $obj[1] . '\',\'' . $url . '\', now());';

    //echo $sql;

    $result = mysqli_query($con, $sql);

}
?>
</ul>

<p>Now run <a href="http://localhost/phd/do-crawl.php?c=<?php echo $c; ?>">http://localhost/phd/do-crawl.php?c=<?php echo $c; ?></a></p>
</div>
</body>
</html>