* some sites are not in the API for certain years: openpolytechnic.ac.nz
* some sites have more than one viewport meta-tag: massey.ac.nz and estacio.br - why is this?
* historial changes of viewport code
* some change name, which is highly annoying: see vuw.ac.nz/victoria university
* bristol university - blocked by robots?

Limitations of the API
* It is difficult to see when universities change name or URI. Often they redirect traffic and this is difficult to detect and test using the content returned from the Internet Archive.

