<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sites from mediaqueri.es</title>
</head>
<body>

<ul>

<?php

$url = "https://mediaqueri.es/";

$arrContextOptions = array(
    "ssl" => array(
        "verify_peer" => false,
        "verify_peer_name" => false,
    ),
);

$all_sites = array();

for ($i = 1; $i < 52; $i++) {

    $response = file_get_contents($url . $i . '/', false, stream_context_create($arrContextOptions));

    array_push($all_sites, $response);

}

$all_sites = implode($all_sites, '');

//preg_match_all($all_sites, '/shots\">\n[ ]*<a href="(.*)\">/g', $urls);

preg_match_all('/shots\">\n[ ]*<a href="(.*)\">/', $all_sites, $urls);

foreach ($urls[1] as $u) {
    echo '<li>' . $u . '</li>';
}

?>
</ul>
</body>
</html>