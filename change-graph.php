<?php

include '_config.php';
include '_functions.php';
include '_global.php';

$overall_count = 0;
$overall_api = 0;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>change over time</title>
    <link rel="stylesheet" type="text/css" href="css/phd.css">

</head>
<body>
<?php menu();?>
<div class="graphs">
<h2>Percentage increase of number of responsive University sites found</h2>

<?php

echo '<ul class="graph no-width">';

$numbers = array(2, 12, 24, 36, 94, 254, 822, 1926, 3324, 4754, 5924);

for ($i = 4; $i < count($numbers) + 1; $i++) {
    if ($i == 6) {$message = '<b style="padding-left: 15px;">Introduction of IE9 and Marcotte&#8217;s book published</b>';} else { $message = '';}
    $percentage = round(($numbers[$i - 1] / $numbers[$i - 2] * 100) - 100, 0);
    echo '<li><span class="year-range">' . ($i + 2005) . ' &#8211; ' . ($i + 2006) . '</span><span class="bar" style="background-color: red; width:' . $percentage . 'px"></span><span class="percentage">' . $percentage . '%</span>' . $message . '</li>';
}

echo '</ul>';

?>

</div>

</body>
</html>
