<?php
$council = <<<COUN
<div class="table-responsive"><table><thead><tr><td><strong>Website</strong></td>
<td><strong>Twitter</strong></td>
<td><strong>Facebook</strong></td>
<td><strong>YouTube</strong></td>
<td><strong>Blog</strong></td>
<td><strong>Other</strong></td>
</tr></thead><tbody><tr><td><strong><a href="http://www.adur-worthing.gov.uk/" rel="noopener noreferrer">Adur and Worthing Borough Council</a></strong></td>
<td><a href="https://twitter.com/adurandWorthing" rel="noopener noreferrer">@adurandWorthing</a></td>
<td><a href="https://www.facebook.com/pages/Adur-and-Worthing/200199263341601" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/AdurandWorthing" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.adur-worthing.gov.uk/" rel="noopener noreferrer">Adur District Council</a></strong></td>
<td><a href="https://twitter.com/adurandWorthing" rel="noopener noreferrer">@adurandWorthing</a></td>
<td><a href="https://www.facebook.com/pages/Adur-and-Worthing/200199263341601" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/AdurandWorthing" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.allerdale.gov.uk/" rel="noopener noreferrer">Allerdale Borough Council</a></strong></td>
<td><a href="https://twitter.com/allerdale" rel="noopener noreferrer">@allerdale</a></td>
<td><a href="https://www.facebook.com/pages/Allerdale-Borough-Council/19233423780" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td><a href="http://www.allerdale.gov.uk/council-and-democracy/leaders-blog.aspx" rel="noopener noreferrer">Leader's blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.ambervalley.gov.uk/" rel="noopener noreferrer">Amber Valley Borough Council</a></strong></td>
<td><a href="https://twitter.com/AmberValleyBC" rel="noopener noreferrer">@AmberValleyBC</a></td>
<td><a href="https://www.facebook.com/AmberValleyBC" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.arun.gov.uk/" rel="noopener noreferrer">Arun District Council</a></strong></td>
<td><a href="https://twitter.com/arundistrict" rel="noopener noreferrer">@arundistrict</a></td>
<td><a href="https://www.facebook.com/arundistrictcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCfJhYHlZdaQ71VjDV4VfjXA" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.ashfield-dc.gov.uk/" rel="noopener noreferrer">Ashfield District Council</a></strong></td>
<td><a href="https://twitter.com/ashfieldcounc" rel="noopener noreferrer">@ashfieldcounc</a></td>
<td><a href="https://www.facebook.com/pages/Ashfield-District-Council/151965718172261" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.ashford.gov.uk/" rel="noopener noreferrer">Ashford Borough Council</a></strong></td>
<td><a href="https://twitter.com/ashfordcouncil" rel="noopener noreferrer">@ashfordcouncil</a></td>
<td> </td>
<td><a href="https://www.youtube.com/channel/UC0Wr-qlbJVmOnt22M_4aeWQ" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.aylesburyvaledc.gov.uk/" rel="noopener noreferrer">Aylesbury Vale District Council</a></strong></td>
<td><a href="https://twitter.com/aylesburyvale" rel="noopener noreferrer">@aylesburyvale</a></td>
<td><a href="https://www.facebook.com/AylesburyVale" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCEf2njuTGUMhvS2XpXyPudA" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.babergh.gov.uk/" rel="noopener noreferrer">Babergh District Council</a></strong></td>
<td><a href="https://twitter.com/BaberghDistrict" rel="noopener noreferrer">@BaberghDistrict</a></td>
<td><a href="https://www.facebook.com/BaberghDistrict" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.barnsley.gov.uk/" rel="noopener noreferrer">Barnsley Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/barnsleycouncil" rel="noopener noreferrer">@barnsleycouncil</a></td>
<td><a href="https://www.facebook.com/BarnsleyCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td><a href="http://new.barnsley.gov.uk/web-services-blog/" rel="noopener noreferrer">Web Services blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.barrowbc.gov.uk/" rel="noopener noreferrer">Barrow-in-Furness Borough Council</a></strong></td>
<td><a href="https://twitter.com/BarrowCouncil" rel="noopener noreferrer">@BarrowCouncil</a></td>
<td><a href="https://www.facebook.com/pages/Barrow-Borough-Council-Housing-Department/377941538953692" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.basildon.gov.uk/" rel="noopener noreferrer">Basildon Borough Council</a></strong></td>
<td><a href="https://twitter.com/basildoncouncil" rel="noopener noreferrer">@basildoncouncil</a></td>
<td><a href="https://www.facebook.com/basildonboroughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/BasildonCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.basingstoke.gov.uk/" rel="noopener noreferrer">Basingstoke and Deane Borough Council</a></strong></td>
<td><a href="https://twitter.com/basingstokegov" rel="noopener noreferrer">@basingstokegov</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.bassetlaw.gov.uk/" rel="noopener noreferrer">Bassetlaw District Council</a></strong></td>
<td><a href="https://twitter.com/bassetlawdc" rel="noopener noreferrer">@bassetlawdc</a></td>
<td><a href="https://www.facebook.com/BassetlawDC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCiByRKT93pMkz6-tjfLOIQw" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.bathnes.gov.uk/" rel="noopener noreferrer">Bath and North East Somerset Council</a></strong></td>
<td><a href="https://twitter.com/bathnes" rel="noopener noreferrer">@bathnes</a></td>
<td><a href="https://www.facebook.com/BathnesFIS" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCAc6JFBe__K5mugdUAHeF9w" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://www.bzine.uk/" rel="noopener noreferrer">Bzine online magazine</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.bedford.gov.uk/" rel="noopener noreferrer">Bedford Borough Council</a></strong></td>
<td><a href="https://twitter.com/bedfordtweets" rel="noopener noreferrer">@bedfordtweets</a></td>
<td><a href="https://www.facebook.com/bedfordboroughcouncil/?ref=aymt_homepage_panel" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCHtgn4fQH1b5c5G69cSI87g" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.birmingham.gov.uk/" rel="noopener noreferrer">Birmingham City Council</a></strong></td>
<td><a href="https://twitter.com/bhamcitycouncil" rel="noopener noreferrer">@bhamcitycouncil</a></td>
<td><a href="https://www.facebook.com/birminghamcitycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/didyouknowbirmingham" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://birminghamnewsroom.com/category/news/" rel="noopener noreferrer">Birmingham Newsroom blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.blaby.gov.uk/" rel="noopener noreferrer">Blaby District Council</a></strong></td>
<td><a href="https://twitter.com/blabydc" rel="noopener noreferrer">@blabydc</a></td>
<td><a href="https://www.facebook.com/blabydc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/BlabyDistrictCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.blackburn.gov.uk/" rel="noopener noreferrer">Blackburn with Darwen Borough Council</a></strong></td>
<td><a href="https://twitter.com/blackburndarwen" rel="noopener noreferrer">@blackburndarwen</a></td>
<td><a href="https://www.facebook.com/theshuttle" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/BWDCtube" rel="noopener noreferrer">YouTube channel</a></td>
<td>
<p><a href="https://blackburnmuseum.wordpress.com/" rel="noopener noreferrer">Blackburn Museum blog</a></p>
<p><a href="http://bwdtransport.blogspot.co.uk/" rel="noopener noreferrer">BWD Transport blog</a></p>
</td>
<td>
<p><a href="https://www.flickr.com/photos/blackburn-with-darwen/" rel="noopener noreferrer">Blackburn with Darwen Council Flickr stream</a></p>
<p><a href="http://www.blackburn.gov.uk/Pages/Social-media.aspx" rel="noopener noreferrer">See all social media channels</a></p>
</td>
</tr><tr><td><strong><a href="http://www.blackpool.gov.uk/" rel="noopener noreferrer">Blackpool Borough Council</a></strong></td>
<td><a href="https://twitter.com/BpoolCouncil" rel="noopener noreferrer">@BpoolCouncil</a></td>
<td><a href="https://www.facebook.com/bpoolcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.blaenau-gwent.gov.uk/" rel="noopener noreferrer">Blaenau Gwent County Borough Council</a></strong></td>
<td><a href="https://twitter.com/BlaenauGwentCBC" rel="noopener noreferrer">@BlaenauGwentCBC</a></td>
<td><a href="https://www.facebook.com/blaenaugwentcbc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/BlaenauGwentCBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/visitblaenaugwent/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.bolsover.gov.uk/" rel="noopener noreferrer">Bolsover District Council</a></strong></td>
<td><a href="https://twitter.com/bolsoverdc" rel="noopener noreferrer">@bolsoverdc</a></td>
<td><a href="https://www.facebook.com/bolsoverdistrict.council" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCz-gbxfVIOQCcCw7cE9yftw" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.bolton.gov.uk/" rel="noopener noreferrer">Bolton Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/boltoncouncil" rel="noopener noreferrer">@boltoncouncil</a></td>
<td><a href="https://www.facebook.com/pages/Bolton-Council/102011083185130" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/boltoncouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.broxbourne.gov.uk/" rel="noopener noreferrer">Borough of Broxbourne</a></strong></td>
<td><a href="https://twitter.com/broxbournebc" rel="noopener noreferrer">@broxbournebc</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/BroxbourneCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/broxbourneboroughcouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.boroughofpoole.com/" rel="noopener noreferrer">Borough of Poole</a></strong></td>
<td><a href="https://twitter.com/BoroughofPoole" rel="noopener noreferrer">@BoroughofPoole</a></td>
<td><a href="https://www.facebook.com/YourBoroughofPoole" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/boroughofpoole" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><a href="https://www.mybostonuk.com/" rel="noopener noreferrer"><strong>Boston Borough Council</strong></a></td>
<td><a href="https://twitter.com/bostonboro" rel="noopener noreferrer">@bostonboro</a></td>
<td><a href="https://www.facebook.com/BostonBoroughCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCsz_igjTc1WQX-EZWY8rQ9w" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.bournemouth.gov.uk/" rel="noopener noreferrer">Bournemouth Borough Council</a></strong></td>
<td><a href="https://twitter.com/bournemouthbc" rel="noopener noreferrer">@bournemouthbc</a></td>
<td><a href="https://www.facebook.com/BournemouthBC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UC5zNVP803JPCFMI2Rw4YcmA" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.bracknell-forest.gov.uk/" rel="noopener noreferrer">Bracknell Forest Council</a></strong></td>
<td><a href="https://twitter.com/bracknellforest" rel="noopener noreferrer">@bracknellforest</a></td>
<td><a href="https://www.facebook.com/bracknellforestcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/BracknellForestC" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="https://digitalservices.bracknell-forest.gov.uk/" rel="noopener noreferrer">Digital Services blog</a></td>
<td><a href="https://www.flickr.com/groups/bracknellforest/pool/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.bradford.gov.uk/" rel="noopener noreferrer">Bradford Metropolitan District Council</a></strong></td>
<td><a href="https://twitter.com/bradfordmdc" rel="noopener noreferrer">@bradfordmdc</a></td>
<td><a href="https://www.facebook.com/bradfordmdc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/bradfordmdcvideo" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="https://bradfordmdc.wordpress.com/" rel="noopener noreferrer">Bradford Council blog</a></td>
<td><a href="https://instagram.com/bradfordmdc" rel="noopener noreferrer">Instagram</a></td>
</tr><tr><td><strong><a href="http://www.braintree.gov.uk/" rel="noopener noreferrer">Braintree District Council</a></strong></td>
<td><a href="https://twitter.com/braintreedc" rel="noopener noreferrer">@braintreedc</a></td>
<td><a href="https://www.facebook.com/braintree.districtcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/BraintreecouncilCM7" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.breckland.gov.uk/" rel="noopener noreferrer">Breckland District Council</a></strong></td>
<td><a href="https://twitter.com/BreckCouncil" rel="noopener noreferrer">@BreckCouncil</a></td>
<td><a href="https://www.facebook.com/pages/Breckland-Council/218563544919554" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.brentwood.gov.uk/" rel="noopener noreferrer">Brentwood Borough Council</a></strong></td>
<td><a href="https://twitter.com/brentwood_bc" rel="noopener noreferrer">@brentwood_bc</a></td>
<td><a href="https://www.facebook.com/brentwoodcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.bridgend.gov.uk/" rel="noopener noreferrer">Bridgend County Borough Council</a></strong></td>
<td><a href="https://twitter.com/BridgendCBC" rel="noopener noreferrer">@BridgendCBC</a></td>
<td><a href="https://www.facebook.com/BridgendCBC" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.brighton-hove.gov.uk/" rel="noopener noreferrer">Brighton and Hove City Council</a></strong></td>
<td><a href="https://twitter.com/brightonhovecc" rel="noopener noreferrer">@brightonhovecc</a></td>
<td><a href="https://www.facebook.com/BrightonandHoveCityCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCg6-4QRIpCdEm8E3IJ7nRmQ" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="http://www.flickr.com/photos/brightonhovecitycouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.bristol.gov.uk/" rel="noopener noreferrer">Bristol City Council</a></strong></td>
<td><a href="https://twitter.com/BristolCouncil" rel="noopener noreferrer">@BristolCouncil</a></td>
<td><a href="https://www.facebook.com/BristolCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/bristolcitycouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.broadland.gov.uk/" rel="noopener noreferrer">Broadland District Council</a></strong></td>
<td><a href="https://twitter.com/broadlanddc" rel="noopener noreferrer">@broadlanddc</a></td>
<td><a href="https://www.facebook.com/BroadlandEvents" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCFuzkOdo-UYVg7Nm8zuTsEg" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.bromsgrove.gov.uk/" rel="noopener noreferrer">Bromsgrove District Council</a></strong></td>
<td><a href="https://twitter.com/bromsgrovedc" rel="noopener noreferrer">@bromsgrovedc</a></td>
<td><a href="https://www.facebook.com/pages/Bromsgrove-District-Council/54505722010" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.broxtowe.gov.uk/" rel="noopener noreferrer">Broxtowe Borough Council</a></strong></td>
<td><a href="https://twitter.com/broxtowebc" rel="noopener noreferrer">@broxtowebc</a></td>
<td><a href="https://www.facebook.com/broxtoweboroughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/BroxtoweBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.buckscc.gov.uk/" rel="noopener noreferrer">Buckinghamshire County Council</a></strong></td>
<td><a href="https://twitter.com/buckscc" rel="noopener noreferrer">@buckscc</a></td>
<td><a href="https://www.facebook.com/pages/Our-Buckinghamshire/118283198190717" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/buckinghamshirecc" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://www.tfbpotholes.blogspot.co.uk/" rel="noopener noreferrer">Pothole blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.burnley.gov.uk/" rel="noopener noreferrer">Burnley Borough Council</a></strong></td>
<td><a href="https://twitter.com/burnleycouncil" rel="noopener noreferrer">@burnleycouncil</a></td>
<td><a href="https://www.facebook.com/burnley.council" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td><a href="http://www.burnley.gov.uk/blogs" rel="noopener noreferrer">Blog</a></td>
<td><a href="https://www.flickr.com/photos/81881029@N04" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.bury.gov.uk/" rel="noopener noreferrer">Bury Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/burycouncil" rel="noopener noreferrer">@burycouncil</a></td>
<td><a href="https://www.facebook.com/BuryCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/playlist?list=PLhnHtjh4YTSFHwlPuxuQRweCNVFp1aH2z" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.caerphilly.gov.uk/" rel="noopener noreferrer">Caerphilly County Borough Council</a></strong></td>
<td><a href="https://twitter.com/CaerphillyCBC" rel="noopener noreferrer">@CaerphillyCBC</a></td>
<td><a href="https://www.facebook.com/yourcaerphilly" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/CaerphillyCBCTV" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://instagram.com/caerphillycbc/" rel="noopener noreferrer">Instagram</a></td>
</tr><tr><td><strong><a href="http://www.calderdale.gov.uk/" rel="noopener noreferrer">Calderdale Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/Calderdale" rel="noopener noreferrer">@Calderdale</a></td>
<td><a href="https://www.facebook.com/calderdale" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="https://www.cambridge.gov.uk/" rel="noopener noreferrer">Cambridge City Council</a></strong></td>
<td><a href="https://twitter.com/camcitco" rel="noopener noreferrer">@camcitco</a></td>
<td><a href="https://www.facebook.com/camcitco" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/camcitco" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="https://cambridgelocalplan.wordpress.com/" rel="noopener noreferrer">Cambridge Local Plan blog</a></td>
<td><a href="https://instagram.com/cambcornex" rel="noopener noreferrer">Instagram</a></td>
</tr><tr><td><strong><a href="http://www.cambridgeshire.gov.uk/" rel="noopener noreferrer">Cambridgeshire County Council</a></strong></td>
<td><a href="https://twitter.com/cambscc" rel="noopener noreferrer">@cambscc</a></td>
<td><a href="https://www.facebook.com/CambridgeshireCC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/CambsCountyCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.cannockchasedc.gov.uk/" rel="noopener noreferrer">Cannock Chase District Council</a></strong></td>
<td><a href="https://twitter.com/cannockchasedc" rel="noopener noreferrer">@cannockchasedc</a></td>
<td><a href="https://www.facebook.com/cannockchasedc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/CannockChaseDC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="https://www.canterbury.gov.uk/" rel="noopener noreferrer">Canterbury City Council</a></strong></td>
<td><a href="https://twitter.com/tweetcanterbury" rel="noopener noreferrer">@tweetcanterbury</a></td>
<td><a href="https://www.facebook.com/CanterburyCityCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/canterburycc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.cardiff.gov.uk/" rel="noopener noreferrer">Cardiff Council</a></strong></td>
<td><a href="https://twitter.com/cardiffcouncil" rel="noopener noreferrer">@cardiffcouncil</a></td>
<td><a href="https://www.facebook.com/pages/The-City-of-Cardiff-Council-Cyngor-Dinas-Caerdydd/520998734707256" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/cardiffcouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="https://cardiffleadersblog.wordpress.com/" rel="noopener noreferrer">The Leader's Blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.carlisle.gov.uk/" rel="noopener noreferrer">Carlisle City Council</a></strong></td>
<td><a href="https://twitter.com/carlislecc" rel="noopener noreferrer">@carlislecc</a></td>
<td><a href="https://www.facebook.com/CarlisleCityCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.carmarthenshire.gov.uk/" rel="noopener noreferrer">Carmarthenshire County Council</a></strong></td>
<td><a href="https://twitter.com/Carmscouncil" rel="noopener noreferrer">@Carmscouncil</a></td>
<td><a href="https://www.facebook.com/CarmarthenshireCC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCnMhstINJ8GW_-qeK8CLdHw" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/carmarthenshirecountycouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.castlepoint.gov.uk/" rel="noopener noreferrer">Castle Point Borough Council</a></strong></td>
<td><a href="https://twitter.com/CastlePointBC" rel="noopener noreferrer">@CastlePointBC</a></td>
<td><a href="https://www.facebook.com/castlepoint.broughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/CastlePointBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.centralbedfordshire.gov.uk/" rel="noopener noreferrer">Central Bedfordshire Council</a></strong></td>
<td><a href="https://twitter.com/letstalkcentral" rel="noopener noreferrer">@letstalkcentral</a></td>
<td><a href="https://www.facebook.com/letstalkcentral" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/centralbedfordshire" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.ceredigion.gov.uk/" rel="noopener noreferrer">Ceredigion County Council</a></strong></td>
<td><a href="https://twitter.com/CeredigionCC" rel="noopener noreferrer">@CeredigionCC</a></td>
<td><a href="https://www.facebook.com/CeredigionCC?_rdr" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCEChFmWo8fWHH9_jiC6NaUg" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.charnwood.gov.uk/" rel="noopener noreferrer">Charnwood Borough Council</a></strong></td>
<td><a href="https://twitter.com/CharnwoodBC" rel="noopener noreferrer">@CharnwoodBC</a></td>
<td><a href="https://www.facebook.com/YourCharnwood" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/charnwoodbc" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://www.charnwoodnews.net/" rel="noopener noreferrer">Charnwood News</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.chelmsford.gov.uk/" rel="noopener noreferrer">Chelmsford City Council</a></strong></td>
<td><a href="https://twitter.com/ChelmsCouncil" rel="noopener noreferrer">@ChelmsCouncil</a></td>
<td><a href="https://www.facebook.com/chelmsfordtheatres" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/ChelmsfordCouncil/videos" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.cheltenham.gov.uk/" rel="noopener noreferrer">Cheltenham Borough Council</a></strong></td>
<td><a href="https://twitter.com/cheltenhamBC" rel="noopener noreferrer">@cheltenhamBC</a></td>
<td><a href="https://www.facebook.com/cheltbc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.cheltenham.gov.uk/videos" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://instagram.com/cheltenhambc" rel="noopener noreferrer">Instagram</a></td>
</tr><tr><td><strong><a href="http://www.cherwell-dc.gov.uk/" rel="noopener noreferrer">Cherwell District Council</a></strong></td>
<td><a href="https://twitter.com/CherwellCouncil" rel="noopener noreferrer">@CherwellCouncil</a></td>
<td><a href="https://www.facebook.com/cherwelldistrictcouncil?ref=stream" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.cheshireeast.gov.uk/" rel="noopener noreferrer">Cheshire East Council (Unitary)</a></strong></td>
<td><a href="https://twitter.com/cheshireeast" rel="noopener noreferrer">@cheshireeast</a></td>
<td><a href="https://www.facebook.com/CheshireEastCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/CheshireEast" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.cheshirewestandchester.gov.uk/" rel="noopener noreferrer">Cheshire West and Chester Council</a></strong></td>
<td><a href="https://twitter.com/Go_CheshireWest" rel="noopener noreferrer">@Go_CheshireWest</a></td>
<td><a href="https://www.facebook.com/cheshirewest" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/cheshirewest/featured" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/cheshirewest" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.chesterfield.gov.uk/" rel="noopener noreferrer">Chesterfield Borough Council</a></strong></td>
<td><a href="https://twitter.com/chesterfieldbc" rel="noopener noreferrer">@chesterfieldbc</a></td>
<td><a href="https://www.facebook.com/ChesterfieldBoroughCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/chesterfielduk" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/chesterfieldbc/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.chichester.gov.uk/" rel="noopener noreferrer">Chichester District Council</a></strong></td>
<td><a href="https://twitter.com/ChichesterDC" rel="noopener noreferrer">@ChichesterDC</a></td>
<td><a href="https://www.facebook.com/ChichesterDistrictCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/ChichesterDCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.chiltern.gov.uk/" rel="noopener noreferrer">Chiltern District Council</a></strong></td>
<td><a href="https://twitter.com/ChilternCouncil" rel="noopener noreferrer">@ChilternCouncil</a></td>
<td><a href="https://www.facebook.com/chilterndistrictcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.chorley.gov.uk/" rel="noopener noreferrer">Chorley Council</a></strong></td>
<td><a href="https://twitter.com/chorleycouncil" rel="noopener noreferrer">@chorleycouncil</a></td>
<td><a href="https://www.facebook.com/chorleycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.dorsetforyou.com/" rel="noopener noreferrer">Christchurch Borough Council</a></strong></td>
<td><a href="https://twitter.com/cb_edd_councils" rel="noopener noreferrer">@cb_edd_councils</a></td>
<td><a href="https://www.facebook.com/dorsetforyou" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.lincoln.gov.uk/" rel="noopener noreferrer">City of Lincoln Council</a></strong></td>
<td><a href="https://twitter.com/lincolncouncil" rel="noopener noreferrer">@lincolncouncil</a></td>
<td><a href="https://www.facebook.com/TheCOLC" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.cityoflondon.gov.uk/" rel="noopener noreferrer">City of London</a></strong></td>
<td><a href="https://twitter.com/cityoflondon" rel="noopener noreferrer">@cityoflondon</a></td>
<td><a href="https://www.facebook.com/CityofLondonCorp" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/CityofLondonvideos" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://audioboom.com/cityoflondon" rel="noopener noreferrer">Audioboom</a></td>
</tr><tr><td><strong><a href="http://www.york.gov.uk/" rel="noopener noreferrer">City of York Council</a></strong></td>
<td><a href="https://twitter.com/cityofyork" rel="noopener noreferrer">@cityofyork</a></td>
<td><a href="https://www.facebook.com/cityofyork" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/cityofyorkcouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/yorkcouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.colchester.gov.uk/" rel="noopener noreferrer">Colchester Borough Council</a></strong></td>
<td><a href="https://twitter.com/yourcolchester" rel="noopener noreferrer">@yourcolchester</a></td>
<td><a href="https://www.facebook.com/enjoycolchester" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/ColchesterCBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.conwy.gov.uk/" rel="noopener noreferrer">Conwy County Borough Council</a></strong></td>
<td><a href="https://twitter.com/ConwyCBC" rel="noopener noreferrer">@ConwyCBC</a></td>
<td><a href="https://www.facebook.com/ConwyCBC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/ConwyWeb" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.copeland.gov.uk/" rel="noopener noreferrer">Copeland Borough Council</a></strong></td>
<td><a href="https://twitter.com/copelandbc" rel="noopener noreferrer">@copelandbc</a></td>
<td><a href="https://www.facebook.com/Copelandboroughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td><a href="https://www.flickr.com/photos/66778487@N04" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.corby.gov.uk/" rel="noopener noreferrer">Corby Borough Council</a></strong></td>
<td><a href="https://twitter.com/CorbyBC" rel="noopener noreferrer">@CorbyBC</a></td>
<td><a href="https://www.facebook.com/CorbyInternationalPool?fref=ts" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.cornwall.gov.uk/" rel="noopener noreferrer">Cornwall Council (Unitary)</a></strong></td>
<td><a href="https://twitter.com/CornwallCouncil" rel="noopener noreferrer">@CornwallCouncil</a></td>
<td><a href="https://www.facebook.com/forCornwall" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/forCornwall" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://vimeo.com/cornwallcouncil" rel="noopener noreferrer">Vimeo</a></td>
</tr><tr><td><strong><a href="http://www.cotswold.gov.uk/" rel="noopener noreferrer">Cotswold District Council</a></strong></td>
<td><a href="https://twitter.com/cotswolddc" rel="noopener noreferrer">@cotswolddc</a></td>
<td><a href="https://www.facebook.com/pages/Cotswold-District-Council/126805000380" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.coventry.gov.uk/" rel="noopener noreferrer">Coventry City Council</a></strong></td>
<td><a href="https://twitter.com/coventrycc" rel="noopener noreferrer">@coventrycc</a></td>
<td><a href="https://www.facebook.com/coventrycc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/coventrycc" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://www.coventry.gov.uk/blog/libraries" rel="noopener noreferrer">Coventry Libraries blog</a></td>
<td><a href="https://www.flickr.com/photos/coventrycc" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.cravendc.gov.uk/" rel="noopener noreferrer">Craven District Council</a></strong></td>
<td><a href="https://twitter.com/CravenCouncil" rel="noopener noreferrer">@CravenCouncil</a></td>
<td><a href="https://www.facebook.com/pages/Craven-District-Council/165525386853146" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCdfb6ZRbYnZ1-rRliLmjUwg" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.crawley.gov.uk/" rel="noopener noreferrer">Crawley Borough Council</a></strong></td>
<td><a href="https://twitter.com/crawleybc" rel="noopener noreferrer">@crawleybc</a></td>
<td><a href="https://www.facebook.com/crawleycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/crawleybc/videos" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.cumbria.gov.uk/" rel="noopener noreferrer">Cumbria County Council</a></strong></td>
<td><a href="https://twitter.com/CumbriaCC" rel="noopener noreferrer">@CumbriaCC</a></td>
<td><a href="https://www.facebook.com/CumbriaCC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/cumbriacountycouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/cumbria_county_council/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.dacorum.gov.uk/" rel="noopener noreferrer">Dacorum Council</a></strong></td>
<td><a href="https://twitter.com/DacorumBC" rel="noopener noreferrer">@DacorumBC</a></td>
<td><a href="https://www.facebook.com/dacorum" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/DacorumBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.linkedin.com/company/dacorum-borough-council" rel="noopener noreferrer">LinkedIn</a></td>
</tr><tr><td><strong><a href="http://www.darlington.gov.uk/" rel="noopener noreferrer">Darlington Borough Council</a></strong></td>
<td><a href="https://twitter.com/darlingtonbc" rel="noopener noreferrer">@darlingtonbc</a></td>
<td><a href="https://www.facebook.com/darlingtonboroughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/DarlingtonBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/darlingtonboroughcouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.dartford.gov.uk/" rel="noopener noreferrer">Dartford Borough Council</a></strong></td>
<td><a href="https://twitter.com/dartfordcouncil" rel="noopener noreferrer">@dartfordcouncil</a></td>
<td> </td>
<td> </td>
<td> </td>
<td><a href="https://uk.linkedin.com/company/dartford-borough-council" rel="noopener noreferrer">LinkedIn</a></td>
</tr><tr><td><strong><a href="http://www.daventrydc.gov.uk/" rel="noopener noreferrer">Daventry District Council</a></strong></td>
<td><a href="https://twitter.com/daventrydc" rel="noopener noreferrer">@daventrydc</a></td>
<td><a href="https://www.facebook.com/daventrydistrict" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/daventrydc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/daventrydc/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.denbighshire.gov.uk/" rel="noopener noreferrer">Denbighshire County Council</a></strong></td>
<td><a href="https://twitter.com/denbighshirecc" rel="noopener noreferrer">@denbighshirecc</a></td>
<td><a href="https://www.facebook.com/denbighshirecountycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/dccwebteam1" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/denbighshirecc/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.derby.gov.uk/" rel="noopener noreferrer">Derby City Council</a></strong></td>
<td><a href="https://twitter.com/derbycc" rel="noopener noreferrer">@derbycc</a></td>
<td><a href="https://www.facebook.com/derbycc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/derbycc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/derbycouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.derbyshire.gov.uk/" rel="noopener noreferrer">Derbyshire County Council</a></strong></td>
<td><a href="https://twitter.com/derbyshirecc" rel="noopener noreferrer">@derbyshirecc</a></td>
<td><a href="https://www.facebook.com/derbyshirecc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/Derbyshirecc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/derbyshirecc" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.derbyshiredales.gov.uk/" rel="noopener noreferrer">Derbyshire Dales District Council</a></strong></td>
<td><a href="https://twitter.com/derbyshiredales" rel="noopener noreferrer">@derbyshiredales</a></td>
<td><a href="https://www.facebook.com/derbyshiredales" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/DerbyshireDalesDC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/derbyshiredales/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.devon.gov.uk/" rel="noopener noreferrer">Devon County Council</a></strong></td>
<td><a href="https://twitter.com/DevonCC" rel="noopener noreferrer">@DevonCC</a></td>
<td><a href="https://www.facebook.com/OfficialDevonCC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/dccwebteam" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.doncaster.gov.uk/" rel="noopener noreferrer">Doncaster Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/MyDoncaster" rel="noopener noreferrer">@MyDoncaster</a></td>
<td><a href="https://www.facebook.com/MyDoncaster" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UChJN8o4iKHJLA3hOlOFJQoA" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://instagram.com/mydoncaster" rel="noopener noreferrer">Instagram</a></td>
</tr><tr><td><strong><a href="http://www.dorsetforyou.com/" rel="noopener noreferrer">Dorset County Council</a></strong></td>
<td><a href="https://twitter.com/dorsetforyou" rel="noopener noreferrer">@dorsetforyou</a></td>
<td><a href="https://www.facebook.com/dorsetforyou" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.dover.gov.uk/" rel="noopener noreferrer">Dover District Council</a></strong></td>
<td><a href="https://twitter.com/doverdc" rel="noopener noreferrer">@doverdc</a></td>
<td><a href="https://www.facebook.com/doverdc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/doverdc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.dudley.gov.uk/" rel="noopener noreferrer">Dudley Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/dudleymbc" rel="noopener noreferrer">@dudleymbc</a></td>
<td><a href="https://www.facebook.com/DudleyBorough" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/dudleymbc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/dudley-council/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.durham.gov.uk/" rel="noopener noreferrer">Durham County Council</a></strong></td>
<td><a href="https://twitter.com/durhamcouncil" rel="noopener noreferrer">@durhamcouncil</a></td>
<td><a href="https://www.facebook.com/durhamcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/DurhamCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.linkedin.com/company/durham-county-council" rel="noopener noreferrer">LinkedIn</a></td>
</tr><tr><td><strong><a href="http://www.eastcambs.gov.uk/" rel="noopener noreferrer">East Cambridgeshire District Council</a></strong></td>
<td><a href="https://twitter.com/eastcambs" rel="noopener noreferrer">@eastcambs</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.eastdevon.gov.uk/" rel="noopener noreferrer">East Devon District Council</a></strong></td>
<td><a href="https://twitter.com/eastdevon" rel="noopener noreferrer">@eastdevon</a></td>
<td><a href="https://www.facebook.com/eastdevon" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/eastdevoncouncil1" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.dorsetforyou.com/" rel="noopener noreferrer">East Dorset District Council</a></strong></td>
<td><a href="https://twitter.com/dorsetforyou" rel="noopener noreferrer">@dorsetforyou</a></td>
<td><a href="https://www.dorsetforyou.com/facebook" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/dorsetforyou" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/dorsetforyou" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.easthants.gov.uk/" rel="noopener noreferrer">East Hampshire District Council</a></strong></td>
<td><a href="https://twitter.com/easthantsdc" rel="noopener noreferrer">@easthantsdc</a></td>
<td><a href="https://www.facebook.com/EastHampshireDistrictCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.eastherts.gov.uk/" rel="noopener noreferrer">East Hertfordshire District Council</a></strong></td>
<td><a href="https://twitter.com/EastHerts" rel="noopener noreferrer">@EastHerts</a></td>
<td><a href="https://www.facebook.com/EastHertsDC" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.e-lindsey.gov.uk/" rel="noopener noreferrer">East Lindsey District Council</a></strong></td>
<td><a href="https://twitter.com/eastlindseydc" rel="noopener noreferrer">@eastlindseydc</a></td>
<td><a href="https://www.facebook.com/eastlindseydistrictcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td><a href="https://www.flickr.com/photos/21611052@N02/sets/72157633280011141/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.east-northamptonshire.gov.uk/" rel="noopener noreferrer">East Northamptonshire Council</a></strong></td>
<td><a href="https://twitter.com/encouncil" rel="noopener noreferrer">@encouncil</a></td>
<td><a href="https://www.facebook.com/pages/Reduce-Reuse-Recycle-with-East-Northamptonshire-Council/179893718711046" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.eastriding.gov.uk/" rel="noopener noreferrer">East Riding of Yorkshire Council</a></strong></td>
<td><a href="https://twitter.com/East_Riding" rel="noopener noreferrer">@East_Riding</a></td>
<td><a href="https://www.facebook.com/eastridingcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/eastridingyorkshire/" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/east-riding/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.eaststaffsbc.gov.uk/" rel="noopener noreferrer">East Staffordshire Borough Council</a></strong></td>
<td><a href="https://twitter.com/eaststaffsbc" rel="noopener noreferrer">@eaststaffsbc</a></td>
<td><a href="https://www.facebook.com/eaststaffsbc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/eaststaffsbc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.eastsussex.gov.uk/" rel="noopener noreferrer">East Sussex County Council</a></strong></td>
<td><a href="https://twitter.com/#!/EastSussexCC" rel="noopener noreferrer">@EastSussexCC</a></td>
<td><a href="https://www.facebook.com/eastsussexcc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/user/EastSussexCC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.eastbourne.gov.uk/" rel="noopener noreferrer">Eastbourne Borough Council</a></strong></td>
<td><a href="https://twitter.com/eastbournebc" rel="noopener noreferrer">@eastbournebc</a></td>
<td><a href="https://www.facebook.com/EastbourneC" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.eastleigh.gov.uk/" rel="noopener noreferrer">Eastleigh Borough Council</a></strong></td>
<td><a href="https://twitter.com/eastleighbc" rel="noopener noreferrer">@eastleighbc</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.eden.gov.uk/" rel="noopener noreferrer">Eden District Council</a></strong></td>
<td><a href="http://twitter.com/edencouncil" rel="noopener noreferrer">@edencouncil</a></td>
<td><a href="http://www.facebook.com/EdenDistrictCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/edendistrictcouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.elmbridge.gov.uk/" rel="noopener noreferrer">Elmbridge Borough Council</a></strong></td>
<td><a href="https://twitter.com/ElmbridgeBC" rel="noopener noreferrer">@ElmbridgeBC</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/ElmbridgeBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/31386220@N07/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.eppingforestdc.gov.uk/" rel="noopener noreferrer">Epping Forest District Council</a></strong></td>
<td><a href="https://twitter.com/eppingforestdc" rel="noopener noreferrer">@eppingforestdc</a></td>
<td><a href="http://www.facebook.com/eppingforestdc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/Eppingforestdc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="http://www.flickr.com/photos/eppingforestdc" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.epsom-ewell.gov.uk/" rel="noopener noreferrer">Epsom and Ewell Borough Council</a></strong></td>
<td><a href="https://twitter.com/EpsomEwellBC" rel="noopener noreferrer">@EpsomEwellBC</a></td>
<td><a href="https://www.facebook.com/EpsomEwellBC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/EpsomEwellBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.erewash.gov.uk/" rel="noopener noreferrer">Erewash Borough Council</a></strong></td>
<td><a href="https://twitter.com/ErewashBC" rel="noopener noreferrer">@ErewashBC</a></td>
<td><a href="https://www.facebook.com/erewashbc" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.essex.gov.uk/Pages/Default.aspx" rel="noopener noreferrer">Essex County Council</a></strong></td>
<td><a href="https://twitter.com/essex_cc" rel="noopener noreferrer">@essex_cc</a></td>
<td><a href="https://www.facebook.com/essexcountycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/EssexCountyCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.linkedin.com/company/essex-county-council" rel="noopener noreferrer">LinkedIn</a></td>
</tr><tr><td><strong><a href="http://www.exeter.gov.uk/" rel="noopener noreferrer">Exeter City Council</a></strong></td>
<td><a href="https://twitter.com/ExeterCouncil" rel="noopener noreferrer">@ExeterCouncil</a></td>
<td><a href="https://www.facebook.com/ExeterCityCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/CityCouncilExeter" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/exetercitycouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.fareham.gov.uk/" rel="noopener noreferrer">Fareham Borough Council</a></strong></td>
<td><a href="https://twitter.com/FarehamBC" rel="noopener noreferrer">@FarehamBC</a></td>
<td><a href="https://www.facebook.com/farehambc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/FarehamBCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/126793216@N08/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.fenland.gov.uk/" rel="noopener noreferrer">Fenland District Council</a></strong></td>
<td><a href="https://twitter.com/FenlandCouncil" rel="noopener noreferrer">@FenlandCouncil</a></td>
<td><a href="https://www.facebook.com/FenlandDistrictCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.flintshire.gov.uk/" rel="noopener noreferrer">Flintshire County Council</a></strong></td>
<td><a href="https://twitter.com/flintshirecc" rel="noopener noreferrer">@flintshirecc</a></td>
<td><a href="http://www.flintshire.gov.uk/en/Resident/Contact-Us/Social-Media.aspx" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/flintshirecc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.forest-heath.gov.uk/" rel="noopener noreferrer">Forest Heath District Council</a></strong></td>
<td><a href="https://twitter.com/forestheath" rel="noopener noreferrer">@forestheath</a></td>
<td><a href="https://www.facebook.com/pages/Forest-Heath-District-Council/441200625920773" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.fdean.gov.uk/" rel="noopener noreferrer">Forest of Dean District Council</a></strong></td>
<td><a href="https://twitter.com/FoDDC" rel="noopener noreferrer">@FoDDC</a></td>
<td><a href="https://www.facebook.com/FODDC" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.fylde.gov.uk/" rel="noopener noreferrer">Fylde Borough Council</a></strong></td>
<td><a href="https://twitter.com/fyldecouncil" rel="noopener noreferrer">@fyldecouncil</a></td>
<td><a href="https://www.facebook.com/fylde" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.gateshead.gov.uk/" rel="noopener noreferrer">Gateshead Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/GMBCouncil" rel="noopener noreferrer">@GMBCouncil</a></td>
<td><a href="https://www.facebook.com/gatesheadcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/GatesheadMBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/gatesheadcouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.gedling.gov.uk/" rel="noopener noreferrer">Gedling Borough Council</a></strong></td>
<td><a href="https://twitter.com/GedlingBC" rel="noopener noreferrer">@GedlingBC</a></td>
<td><a href="https://www.facebook.com/gedlingborough" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td><a href="https://www.flickr.com/photos/gedlingbc" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.gloucester.gov.uk/" rel="noopener noreferrer">Gloucester City Council</a></strong></td>
<td><a href="https://twitter.com/GloucesterCity" rel="noopener noreferrer">@GloucesterCity</a></td>
<td><a href="https://www.facebook.com/pages/Gloucester-City-Council/377737475930" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/GloucesterCity" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.gloucestershire.gov.uk/" rel="noopener noreferrer">Gloucestershire County Council</a></strong></td>
<td><a href="https://twitter.com/GlosCC" rel="noopener noreferrer">@GlosCC</a></td>
<td><a href="https://www.facebook.com/pages/Gloucestershire-County-Council/242363598572" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/GloucestershireCC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.gosport.gov.uk/" rel="noopener noreferrer">Gosport Borough Council</a></strong></td>
<td><a href="https://twitter.com/GosportCouncil" rel="noopener noreferrer">@GosportCouncil</a></td>
<td><a href="https://www.facebook.com/GosportBC" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.gravesham.gov.uk/" rel="noopener noreferrer">Gravesham Borough Council</a></strong></td>
<td><a href="https://twitter.com/graveshambc" rel="noopener noreferrer">@graveshambc</a></td>
<td><a href="https://www.facebook.com/GraveshamBoroughCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/gravesham" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/gbccomms" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.great-yarmouth.gov.uk/" rel="noopener noreferrer">Great Yarmouth Borough Council</a></strong></td>
<td><a href="https://twitter.com/greatyarmouthbc" rel="noopener noreferrer">@greatyarmouthbc</a></td>
<td><a href="https://www.facebook.com/pages/Great-Yarmouth-Borough-Council/127133594010506" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.guildford.gov.uk/" rel="noopener noreferrer">Guildford Borough Council</a></strong></td>
<td><a href="https://twitter.com/guildfordbc" rel="noopener noreferrer">@guildfordbc</a></td>
<td><a href="https://www.facebook.com/GuildfordBC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/GuildfordBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.gwynedd.gov.uk/" rel="noopener noreferrer">Gwynedd County Council</a></strong></td>
<td><a href="https://twitter.com/cyngorgwynedd" rel="noopener noreferrer">@cyngorgwynedd</a></td>
<td><a href="https://www.facebook.com/CyngorGwyneddCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/CyngorGwynedd" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.halton.gov.uk/" rel="noopener noreferrer">Halton Borough Council</a></strong></td>
<td><a href="https://twitter.com/haltonbc" rel="noopener noreferrer">@haltonbc</a></td>
<td><a href="https://www.facebook.com/haltonbc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/HaltonCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.pinterest.com/haltonbc/" rel="noopener noreferrer">Pinterest</a></td>
</tr><tr><td><strong><a href="http://www.hambleton.gov.uk/" rel="noopener noreferrer">Hambleton District Council</a></strong></td>
<td><a href="https://twitter.com/HambletonDC" rel="noopener noreferrer">@HambletonDC</a></td>
<td><a href="https://www.facebook.com/hambletondc" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.hants.gov.uk/" rel="noopener noreferrer">Hampshire County Council</a></strong></td>
<td><a href="https://twitter.com/hantsconnect" rel="noopener noreferrer">@hantsconnect</a></td>
<td><a href="https://www.facebook.com/pages/Hampshire-County-Council/152625604758775" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCZUGlVUya3TWSHb9sWaVNqA" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.harborough.gov.uk/" rel="noopener noreferrer">Harborough District Council</a></strong></td>
<td><a href="https://twitter.com/HarboroughDC" rel="noopener noreferrer">@HarboroughDC</a></td>
<td><a href="https://www.facebook.com/harborough.council" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.harlow.gov.uk/" rel="noopener noreferrer">Harlow Council</a></strong></td>
<td><a href="https://twitter.com/harlowcouncil" rel="noopener noreferrer">@harlowcouncil</a></td>
<td><a href="https://www.facebook.com/harlowcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/HarlowCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.harrogate.gov.uk/" rel="noopener noreferrer">Harrogate Borough Council</a></strong></td>
<td><a href="https://twitter.com/Harrogatebc" rel="noopener noreferrer">@Harrogatebc</a></td>
<td><a href="https://www.facebook.com/pages/Harrogate-Borough-Council/289244321239836" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.hart.gov.uk/" rel="noopener noreferrer">Hart District Council</a></strong></td>
<td><a href="https://twitter.com/hartcouncil" rel="noopener noreferrer">@hartcouncil</a></td>
<td><a href="https://www.facebook.com/HartDistrictCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCb58msDJyU2wnf0j84VkuZA" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.hartlepool.gov.uk/" rel="noopener noreferrer">Hartlepool Borough Council</a></strong></td>
<td><a href="https://twitter.com/hpoolcouncil" rel="noopener noreferrer">@hpoolcouncil</a></td>
<td><a href="https://www.facebook.com/hartlepoolcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCJb6kkPvQV_X5s3tJ0KweUw" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.hastings.gov.uk/" rel="noopener noreferrer">Hastings Borough Council</a></strong></td>
<td><a href="https://twitter.com/hastingsbc" rel="noopener noreferrer">@hastingsbc</a></td>
<td><a href="https://www.facebook.com/Hastingsboroughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/HastingsCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://leaderofhastingsboroughcouncil.blogspot.co.uk/" rel="noopener noreferrer">Leader's blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.havant.gov.uk/" rel="noopener noreferrer">Havant Borough Council</a></strong></td>
<td><a href="https://twitter.com/havantborough" rel="noopener noreferrer">@havantborough</a></td>
<td><a href="https://www.facebook.com/HavantBoroughCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="https://www.herefordshire.gov.uk/" rel="noopener noreferrer">Herefordshire Council</a></strong></td>
<td><a href="https://twitter.com/HfdsCouncil" rel="noopener noreferrer">@HfdsCouncil</a></td>
<td><a href="https://www.facebook.com/hfdscouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/herefordshirecouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="http://www.flickr.com/photos/hfdscouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.hertsdirect.org/" rel="noopener noreferrer">Hertfordshire County Council</a></strong></td>
<td><a href="https://twitter.com/hertscc" rel="noopener noreferrer">@hertscc</a></td>
<td><a href="https://www.facebook.com/hertscountycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/hertscountycouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.hertsmere.gov.uk/" rel="noopener noreferrer">Hertsmere Borough Council</a></strong></td>
<td><a href="https://twitter.com/HertsmereBC" rel="noopener noreferrer">@HertsmereBC</a></td>
<td><a href="https://www.facebook.com/hertsmere" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/HertsmereBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/hertsmere" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.highpeak.gov.uk/" rel="noopener noreferrer">High Peak Borough Council</a></strong></td>
<td><a href="https://twitter.com/HighPeakBC" rel="noopener noreferrer">@HighPeakBC</a></td>
<td><a href="https://www.facebook.com/pages/Whaley-Bridge-Memorial-Park/119442508158140" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.hinckley-bosworth.gov.uk/" rel="noopener noreferrer">Hinckley and Bosworth Borough Council</a></strong></td>
<td><a href="https://twitter.com/hinckandbos_bc" rel="noopener noreferrer">@hinckandbos_bc</a></td>
<td><a href="https://www.facebook.com/hinckandbosbc" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.horsham.gov.uk/" rel="noopener noreferrer">Horsham District Council</a></strong></td>
<td><a href="https://twitter.com/HorshamDC" rel="noopener noreferrer">@HorshamDC</a></td>
<td><a href="https://www.facebook.com/Horsham.District.Council" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/HorshamDC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.huntingdonshire.gov.uk/" rel="noopener noreferrer">Huntingdonshire District Council</a></strong></td>
<td><a href="https://twitter.com/huntsdc" rel="noopener noreferrer">@huntsdc</a></td>
<td><a href="http://www.facebook.com/huntingdonshire" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/HuntingdonshireDC/videos" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="http://www.flickr.com/photos/31192642@N06/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.hyndburnbc.gov.uk/" rel="noopener noreferrer">Hyndburn Borough Council</a></strong></td>
<td><a href="https://twitter.com/HyndburnCouncil" rel="noopener noreferrer">@HyndburnCouncil</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="https://www.ipswich.gov.uk/" rel="noopener noreferrer">Ipswich Borough Council</a></strong></td>
<td><a href="https://twitter.com/IpswichGov" rel="noopener noreferrer">@IpswichGov</a></td>
<td><a href="https://www.facebook.com/IpswichGov" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.anglesey.gov.uk/" rel="noopener noreferrer">Isle of Anglesey County Council</a></strong></td>
<td><a href="https://twitter.com/angleseycouncil" rel="noopener noreferrer">@angleseycouncil</a></td>
<td><a href="https://www.facebook.com/IOACC" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.iwight.com/" rel="noopener noreferrer">Isle of Wight Council</a></strong></td>
<td><a href="https://twitter.com/iwight" rel="noopener noreferrer">@iwight</a></td>
<td><a href="https://www.facebook.com/isleofwightcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.scilly.gov.uk/" rel="noopener noreferrer">Isles of Scilly</a></strong></td>
<td><a href="https://twitter.com/ioscouncil" rel="noopener noreferrer">@ioscouncil</a></td>
<td><a href="https://www.facebook.com/ioscouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.kent.gov.uk/" rel="noopener noreferrer">Kent County Council</a></strong></td>
<td><a href="http://www.twitter.com/Kent_cc" rel="noopener noreferrer">@Kent_cc</a></td>
<td><a href="https://www.facebook.com/kentyouthcountycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.kettering.gov.uk/" rel="noopener noreferrer">Kettering Borough Council</a></strong></td>
<td><a href="https://twitter.com/KetteringBC" rel="noopener noreferrer">@KetteringBC</a></td>
<td><a href="https://www.facebook.com/KetteringBorough" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/KetteringBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/ketteringboroughcouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.west-norfolk.gov.uk/" rel="noopener noreferrer">King's Lynn and West Norfolk Borough Council</a></strong></td>
<td><a href="http://twitter.com/WestNorfolkBC" rel="noopener noreferrer">@WestNorfolkBC</a></td>
<td><a href="https://www.facebook.com/pages/Alive-Corn-Exchange/114233291979729" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/westnorfolkbc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.hullcc.gov.uk/" rel="noopener noreferrer">Kingston-upon-Hull City Council</a></strong></td>
<td><a href="https://twitter.com/Hullccnews" rel="noopener noreferrer">@Hullccnews</a></td>
<td><a href="https://www.facebook.com/hullevents" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/hullcitycouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.kirklees.gov.uk/" rel="noopener noreferrer">Kirklees Council</a></strong></td>
<td><a href="http://twitter.com/KirkleesCouncil" rel="noopener noreferrer">@KirkleesCouncil</a></td>
<td><a href="http://www.facebook.com/liveinkirklees" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.knowsley.gov.uk/" rel="noopener noreferrer">Knowsley Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/KnowsleyCouncil" rel="noopener noreferrer">@KnowsleyCouncil</a></td>
<td><a href="https://www.facebook.com/knowsley.council" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.lancashire.gov.uk/" rel="noopener noreferrer">Lancashire County Council</a></strong></td>
<td><a href="http://www.twitter.com/LancashireCC" rel="noopener noreferrer">@LancashireCC</a></td>
<td><a href="http://www.facebook.com/lancashirecc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/user/lancashirecc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/lancashirecc/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.lancaster.gov.uk/" rel="noopener noreferrer">Lancaster City Council</a></strong></td>
<td><a href="http://www.twitter.com/lancastercc" rel="noopener noreferrer">@lancastercc</a></td>
<td><a href="http://www.facebook.com/lancastercc" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.leeds.gov.uk/" rel="noopener noreferrer">Leeds City Council</a></strong></td>
<td><a href="https://twitter.com/LeedsCC_Help" rel="noopener noreferrer">@LeedsCC_Help</a></td>
<td><a href="http://www.facebook.com/Leedscouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/leedscouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.leicester.gov.uk/" rel="noopener noreferrer">Leicester City Council</a></strong></td>
<td><a href="http://twitter.com/Leicester_News" rel="noopener noreferrer">@Leicester_News</a></td>
<td><a href="http://www.facebook.com/pages/Leicester-City-Council/107990299249901" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.leics.gov.uk/" rel="noopener noreferrer">Leicestershire County Council</a></strong></td>
<td><a href="http://twitter.com/LeicsCountyHall" rel="noopener noreferrer">@LeicsCountyHall</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.lewes.gov.uk/" rel="noopener noreferrer">Lewes District Council</a></strong></td>
<td><a href="https://twitter.com/lewesdc" rel="noopener noreferrer">@lewesdc</a></td>
<td><a href="https://www.facebook.com/lewesdistrictcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.lichfielddc.gov.uk/" rel="noopener noreferrer">Lichfield District Council</a></strong></td>
<td><a href="https://twitter.com/Lichfield_DC" rel="noopener noreferrer">@Lichfield_DC</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.lincolnshire.gov.uk/" rel="noopener noreferrer">Lincolnshire County Council</a></strong></td>
<td><a href="https://twitter.com/LincolnshireCC" rel="noopener noreferrer">@LincolnshireCC</a></td>
<td><a href="https://www.facebook.com/#!/lincolnshirecc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/user/LincolnshireCC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.liverpool.gov.uk/" rel="noopener noreferrer">Liverpool City Council</a></strong></td>
<td><a href="https://twitter.com/lpoolcouncil" rel="noopener noreferrer">@lpoolcouncil</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.lbbd.gov.uk/" rel="noopener noreferrer">London Borough of Barking and Dagenham</a></strong></td>
<td><a href="https://twitter.com/lbbdcouncil" rel="noopener noreferrer">@lbbdcouncil</a></td>
<td><a href="https://www.facebook.com/barkinganddagenham" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.barnet.gov.uk/" rel="noopener noreferrer">London Borough of Barnet</a></strong></td>
<td><a href="https://twitter.com/barnetcouncil" rel="noopener noreferrer">@barnetcouncil</a></td>
<td><a href="https://www.facebook.com/pages/Barnet-Council/25963519357" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.bexley.gov.uk/" rel="noopener noreferrer">London Borough of Bexley</a></strong></td>
<td><a href="https://twitter.com/LBofBexley" rel="noopener noreferrer">@LBofBexley</a></td>
<td><a href="https://www.facebook.com/lbbexley" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/LBofBexley" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://www.bexley.gov.uk/index.aspx?articleid=6783" rel="noopener noreferrer">News blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.brent.gov.uk/" rel="noopener noreferrer">London Borough of Brent</a></strong></td>
<td><a href="https://twitter.com/Brent_Council" rel="noopener noreferrer">@Brent_Council</a></td>
<td><a href="https://www.facebook.com/BrentCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/BrentCouncilLondon" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.bromley.gov.uk/" rel="noopener noreferrer">London Borough of Bromley</a></strong></td>
<td><a href="https://twitter.com/lbofbromley" rel="noopener noreferrer">@lbofbromley</a></td>
<td><a href="https://www.facebook.com/LBBromley" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCOV54koqk_Z42ckas7pmZLg" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/lbofbromley" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.camden.gov.uk/" rel="noopener noreferrer">London Borough of Camden</a></strong></td>
<td><a href="https://twitter.com/camdentalking" rel="noopener noreferrer">@camdentalking</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/CamdenCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/camdencouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.croydon.gov.uk/" rel="noopener noreferrer">London Borough of Croydon</a></strong></td>
<td><a href="https://twitter.com/yourcroydon" rel="noopener noreferrer">@yourcroydon</a></td>
<td><a href="https://www.facebook.com/ilovecroydon" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/croydoncouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.ealing.gov.uk/" rel="noopener noreferrer">London Borough of Ealing</a></strong></td>
<td><a href="https://twitter.com/ealingcouncil" rel="noopener noreferrer">@ealingcouncil</a></td>
<td><a href="https://www.facebook.com/pages/Ealing-Council/112242018837465" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCoI9fzIsjNbPOOMnTO6fxhQ" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.enfield.gov.uk/" rel="noopener noreferrer">London Borough of Enfield</a></strong></td>
<td><a href="https://twitter.com/EnfieldCouncil" rel="noopener noreferrer">@EnfieldCouncil</a></td>
<td><a href="https://www.facebook.com/pages/Enfield-Council/252946378095154" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/EnfieldCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/enfieldcouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.hackney.gov.uk/" rel="noopener noreferrer">London Borough of Hackney</a></strong></td>
<td><a href="https://twitter.com/hackneycouncil" rel="noopener noreferrer">@hackneycouncil</a></td>
<td><a href="https://www.facebook.com/hackneycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/hackneycouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/hackneycouncil/collections/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.lbhf.gov.uk/" rel="noopener noreferrer">London Borough of Hammersmith &amp; Fulham</a></strong></td>
<td><a href="https://twitter.com/lbhf" rel="noopener noreferrer">@lbhf</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/hammersmithandfulham" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/hammersmithandfulham/sets/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.haringey.gov.uk/" rel="noopener noreferrer">London Borough of Haringey</a></strong></td>
<td><a href="https://twitter.com/haringeycouncil" rel="noopener noreferrer">@haringeycouncil</a></td>
<td><a href="https://www.facebook.com/haringeycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/haringeycouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/haringey" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.harrow.gov.uk/" rel="noopener noreferrer">London Borough of Harrow</a></strong></td>
<td><a href="https://twitter.com/harrow_council" rel="noopener noreferrer">@harrow_council</a></td>
<td><a href="https://www.facebook.com/lbharrow" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.havering.gov.uk/" rel="noopener noreferrer">London Borough of Havering</a></strong></td>
<td><a href="https://twitter.com/LBofHavering" rel="noopener noreferrer">@LBofHavering</a></td>
<td><a href="https://www.facebook.com/haveringevents" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UC4ZDu-tDYodP1amD2UDQY6w/feed" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://instagram.com/lbofhavering/" rel="noopener noreferrer">Instagram</a></td>
</tr><tr><td><strong><a href="http://www.hillingdon.gov.uk/" rel="noopener noreferrer">London Borough of Hillingdon</a></strong></td>
<td><a href="https://twitter.com/hillingdon" rel="noopener noreferrer">@hillingdon</a></td>
<td><a href="https://www.facebook.com/pages/London-Borough-of-Hillingdon/312443478074" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/hillingdonlondon" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/hillingdon" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.hounslow.gov.uk/" rel="noopener noreferrer">London Borough of Hounslow</a></strong></td>
<td><a href="https://twitter.com/LBofHounslow" rel="noopener noreferrer">@LBofHounslow</a></td>
<td><a href="http://www.facebook.com/Hounslowcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/user/LBHounslow/videos" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/hounslowcouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.islington.gov.uk/Pages/default.aspx" rel="noopener noreferrer">London Borough of Islington</a></strong></td>
<td><a href="http://www.twitter.com/IslingtonBC" rel="noopener noreferrer">@IslingtonBC</a></td>
<td><a href="https://www.facebook.com/recyclingislington" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/islingtontube" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="http://www.flickr.com/photos/islingtonlife/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.lambeth.gov.uk/" rel="noopener noreferrer">London Borough of Lambeth</a></strong></td>
<td><a href="https://twitter.com/lambeth_council" rel="noopener noreferrer">@lambeth_council</a></td>
<td><a href="https://www.facebook.com/pages/Lambeth-Council/134678860688" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/user/LambethCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="http://www.flickr.com/photos/lambethcouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.lewisham.gov.uk/" rel="noopener noreferrer">London Borough of Lewisham</a></strong></td>
<td><a href="http://twitter.com/LewishamCouncil" rel="noopener noreferrer">@LewishamCouncil</a></td>
<td><a href="https://www.facebook.com/pages/Lewisham-Council/127390450064" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/LewishamCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.merton.gov.uk/" rel="noopener noreferrer">London Borough of Merton</a></strong></td>
<td><a href="https://twitter.com/merton_council" rel="noopener noreferrer">@merton_council</a></td>
<td><a href="https://www.facebook.com/mertoncouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/MertonCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/mertoncouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.newham.gov.uk/" rel="noopener noreferrer">London Borough of Newham</a></strong></td>
<td><a href="https://twitter.com/NewhamLondon" rel="noopener noreferrer">@NewhamLondon</a></td>
<td><a href="https://www.facebook.com/newhamcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/LBNewham" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.redbridge.gov.uk/" rel="noopener noreferrer">London Borough of Redbridge</a></strong></td>
<td><a href="https://twitter.com/redbridgelive" rel="noopener noreferrer">@redbridgelive</a></td>
<td><a href="https://www.facebook.com/RedbridgeLive" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/Redbridgei" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/groups/redbridge/pool/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.richmond.gov.uk/" rel="noopener noreferrer">London Borough of Richmond upon Thames</a></strong></td>
<td><a href="https://twitter.com/lbrut" rel="noopener noreferrer">@lbrut</a></td>
<td><a href="https://www.facebook.com/lbrut" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCE8AZP0xC-hcPEUD_NUAoZQ" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/lbrut/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.southwark.gov.uk/" rel="noopener noreferrer">London Borough of Southwark</a></strong></td>
<td><a href="https://twitter.com/lb_southwark" rel="noopener noreferrer">@lb_southwark</a></td>
<td><a href="https://www.facebook.com/southwarkcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/southwarkcouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.sutton.gov.uk/" rel="noopener noreferrer">London Borough of Sutton</a></strong></td>
<td><a href="https://twitter.com/suttoncouncil" rel="noopener noreferrer">@suttoncouncil</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/Suttoncouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.towerhamlets.gov.uk/" rel="noopener noreferrer">London Borough of Tower Hamlets</a></strong></td>
<td><a href="https://twitter.com/TowerHamletsNow" rel="noopener noreferrer">@TowerHamletsNow</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/towerhamletscouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.walthamforest.gov.uk/" rel="noopener noreferrer">London Borough of Waltham Forest</a></strong></td>
<td><a href="https://twitter.com/wfcouncil" rel="noopener noreferrer">@wfcouncil</a></td>
<td><a href="https://www.facebook.com/pages/London-Borough-of-Waltham-Forest-Democracy/505645389479076" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UC_LE8dzwfyl3N_IEe919zEw" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.wandsworth.gov.uk/" rel="noopener noreferrer">London Borough of Wandsworth</a></strong></td>
<td><a href="https://twitter.com/wandbc" rel="noopener noreferrer">@wandbc</a></td>
<td><a href="https://www.facebook.com/wandsworth.council" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/WandsworthBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://uk.linkedin.com/company/wandsworth-borough-council" rel="noopener noreferrer">LinkedIn</a></td>
</tr><tr><td><strong><a href="http://www.luton.gov.uk/" rel="noopener noreferrer">Luton Borough Council</a></strong></td>
<td><a href="http://www.twitter.com/lutoncouncil" rel="noopener noreferrer">@lutoncouncil</a></td>
<td><a href="http://www.facebook.com/lutoncouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/lutonboroughcouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="http://www.flickr.com/photos/lutonboroughcouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.maidstone.gov.uk/" rel="noopener noreferrer">Maidstone Borough Council</a></strong></td>
<td><a href="https://twitter.com/maidstonebc" rel="noopener noreferrer">@maidstonebc</a></td>
<td><a href="http://www.maidstone.gov.uk/residents/news/facebook" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.maldon.gov.uk/" rel="noopener noreferrer">Maldon District Council</a></strong></td>
<td><a href="http://twitter.com/maldondc" rel="noopener noreferrer">@maldondc</a></td>
<td> </td>
<td> </td>
<td> </td>
<td><a href="http://flickr.com/photos/maldondc" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.malvernhills.gov.uk/" rel="noopener noreferrer">Malvern Hills District Council</a></strong></td>
<td><a href="https://twitter.com/MHDCcomms" rel="noopener noreferrer">@MHDCcomms</a></td>
<td><a href="https://www.facebook.com/MalvernHillsUK" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UC6DjhYJweGyyuc3jeSP15CQ" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.manchester.gov.uk/" rel="noopener noreferrer">Manchester City Council</a></strong></td>
<td><a href="https://twitter.com/ManCityCouncil" rel="noopener noreferrer">@ManCityCouncil</a></td>
<td><a href="http://www.facebook.com/mancitycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td><a href="http://www.manchester.gov.uk/blog/leadersblog/post/728/-invisibility" rel="noopener noreferrer">The Leader's Blog</a></td>
<td><a href="http://www.flickr.com/photos/manchester-city-council/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.mansfield.gov.uk/" rel="noopener noreferrer">Mansfield District Council</a></strong></td>
<td><a href="https://twitter.com/MDC_News" rel="noopener noreferrer">@MDC_News</a></td>
<td><a href="http://www.facebook.com/MyMansfieldUK" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.medway.gov.uk/" rel="noopener noreferrer">Medway Council</a></strong></td>
<td><a href="https://twitter.com/medway_council" rel="noopener noreferrer">@medway_council</a></td>
<td><a href="https://www.facebook.com/pages/Medway-Council-Service-updates-and-information/446064705414795" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/user/medwaycouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="http://www.flickr.com/photos/medwaycouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.melton.gov.uk/" rel="noopener noreferrer">Melton Borough Council</a></strong></td>
<td><a href="https://twitter.com/MeltonBC" rel="noopener noreferrer">@MeltonBC</a></td>
<td><a href="https://www.facebook.com/meltonbc" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.mendip.gov.uk/" rel="noopener noreferrer">Mendip District Council</a></strong></td>
<td><a href="https://twitter.com/MendipCouncil" rel="noopener noreferrer">@MendipCouncil</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.merthyr.gov.uk/" rel="noopener noreferrer">Merthyr Tydfil County Borough Council</a></strong></td>
<td><a href="https://twitter.com/merthyrcbc" rel="noopener noreferrer">@merthyrcbc</a></td>
<td><a href="https://www.facebook.com/merthyrtydfilcbc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/MerthyrTydfilCBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.middevon.gov.uk/" rel="noopener noreferrer">Mid Devon District Council</a></strong></td>
<td><a href="https://twitter.com/MidDevonDC" rel="noopener noreferrer">@MidDevonDC</a></td>
<td><a href="https://www.facebook.com/middevon1" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.midsuffolk.gov.uk/" rel="noopener noreferrer">Mid Suffolk District Council</a></strong></td>
<td><a href="https://twitter.com/midsuffolk" rel="noopener noreferrer">@midsuffolk</a></td>
<td><a href="https://www.facebook.com/midsuffolkdistrictcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.midsussex.gov.uk/" rel="noopener noreferrer">Mid Sussex District Council</a></strong></td>
<td><a href="https://twitter.com/MSDCnews" rel="noopener noreferrer">@MSDCnews</a></td>
<td><a href="https://www.facebook.com/midsussexdistrictcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.middlesbrough.gov.uk/" rel="noopener noreferrer">Middlesbrough Borough Council</a></strong></td>
<td><a href="https://twitter.com/MbroCouncil" rel="noopener noreferrer">@MbroCouncil</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/middlesbroughcouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/middlesbroughcouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.milton-keynes.gov.uk/" rel="noopener noreferrer">Milton Keynes</a></strong></td>
<td><a href="https://twitter.com/mkcouncil" rel="noopener noreferrer">@mkcouncil</a></td>
<td><a href="https://www.facebook.com/MiltonKeynesCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/MiltonKeynesCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.linkedin.com/company/milton-keynes-council" rel="noopener noreferrer">LinkedIn</a></td>
</tr><tr><td><strong><a href="http://www.molevalley.gov.uk/" rel="noopener noreferrer">Mole Valley District Council</a></strong></td>
<td><a href="https://twitter.com/MoleValleyDC" rel="noopener noreferrer">@MoleValleyDC</a></td>
<td><a href="https://www.facebook.com/MoleValleyDistrictCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/molevalleydc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.monmouthshire.gov.uk/" rel="noopener noreferrer">Monmouthshire County Council</a></strong></td>
<td><a href="https://twitter.com/monmouthshirecc" rel="noopener noreferrer">@monmouthshirecc</a></td>
<td><a href="https://www.facebook.com/MonmouthshireCC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCZHCKKCl7DqtxDabOkj_Esg" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.neath-porttalbot.gov.uk/" rel="noopener noreferrer">Neath Port Talbot County Borough Council</a></strong></td>
<td><a href="https://twitter.com/NPTCouncil" rel="noopener noreferrer">@NPTCouncil</a></td>
<td><a href="https://www.facebook.com/NeathPortTalbotCBC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/NeathPortTalbotCBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.newforestdc.gov.uk/" rel="noopener noreferrer">New Forest District Council</a></strong></td>
<td><a href="https://twitter.com/newforestdc" rel="noopener noreferrer">@newforestdc</a></td>
<td><a href="https://www.facebook.com/newforestgov" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.newark-sherwooddc.gov.uk/" rel="noopener noreferrer">Newark and Sherwood District Council</a></strong></td>
<td><a href="https://twitter.com/NSDCouncil" rel="noopener noreferrer">@NSDCouncil</a></td>
<td><a href="https://www.facebook.com/pages/Newark-and-Sherwood-District-Council/327517440618344" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/NSDCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.newcastle-staffs.gov.uk/" rel="noopener noreferrer">Newcastle-Under-Lyme District Council</a></strong></td>
<td><a href="https://twitter.com/NewsNBC" rel="noopener noreferrer">@NewsNBC</a></td>
<td><a href="https://www.facebook.com/newcastleunderlyme" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.newport.gov.uk/en/Home.aspx" rel="noopener noreferrer">Newport City Council</a></strong></td>
<td><a href="http://twitter.com/newportcouncil" rel="noopener noreferrer">@NewportCouncil</a></td>
<td><a href="http://www.facebook.com/NewportCityCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.newcastle.gov.uk/" rel="noopener noreferrer">Newcastle-upon-Tyne City Council</a></strong></td>
<td><a href="https://twitter.com/NewcastleCC" rel="noopener noreferrer">@NewcastleCC</a></td>
<td><a href="https://www.facebook.com/NewcastleCityCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/NewcastleCCUK" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.linkedin.com/groups/Newcastle-City-Council-4485877?trk=my_groups-b-grp-v" rel="noopener noreferrer">LinkedIn</a></td>
</tr><tr><td><strong><a href="http://www.norfolk.gov.uk/" rel="noopener noreferrer">Norfolk County Council</a></strong></td>
<td><a href="https://twitter.com/norfolkcc" rel="noopener noreferrer">@norfolkcc</a></td>
<td><a href="https://www.facebook.com/Norfolkcc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/NorfolkCountyCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/norfolkcc" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.northdevon.gov.uk/" rel="noopener noreferrer">North Devon Council</a></strong></td>
<td><a href="https://twitter.com/ndevoncouncil" rel="noopener noreferrer">@ndevoncouncil</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.dorsetforyou.com/" rel="noopener noreferrer">North Dorset District Council</a></strong></td>
<td><a href="https://twitter.com/dorsetforyou" rel="noopener noreferrer">@dorsetforyou</a></td>
<td><a href="https://www.facebook.com/dorsetforyou" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/dorsetforyou" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/dorsetforyou" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.ne-derbyshire.gov.uk/" rel="noopener noreferrer">North East Derbyshire District Council</a></strong></td>
<td><a href="https://twitter.com/nedDC" rel="noopener noreferrer">@nedDC</a></td>
<td><a href="https://www.facebook.com/pages/North-East-Derbyshire-District-Council/126052857447586" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.nelincs.gov.uk/" rel="noopener noreferrer">North East Lincolnshire Council</a></strong></td>
<td><a href="https://twitter.com/NELincs" rel="noopener noreferrer">@NELincs</a></td>
<td><a href="https://www.facebook.com/northeastlincolnshirecouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.north-herts.gov.uk/" rel="noopener noreferrer">North Hertfordshire District Council</a></strong></td>
<td><a href="https://twitter.com/northhertsdc" rel="noopener noreferrer">@northhertsdc</a></td>
<td><a href="https://www.facebook.com/northhertsDC" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td><a href="https://www.flickr.com/photos/northhertfordshire/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.n-kesteven.gov.uk/" rel="noopener noreferrer">North Kesteven District Council</a></strong></td>
<td><a href="https://twitter.com/northkestevendc" rel="noopener noreferrer">@northkestevendc</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.northlincs.gov.uk/" rel="noopener noreferrer">North Lincolnshire Council</a></strong></td>
<td><a href="https://twitter.com/NorthLincsCNews" rel="noopener noreferrer">@NorthLincsCNews</a></td>
<td><a href="https://www.facebook.com/northlincscouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/northlincscouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/northlincscouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.northnorfolk.org/" rel="noopener noreferrer">North Norfolk District Council</a></strong></td>
<td><a href="https://twitter.com/NorthNorfolkDC" rel="noopener noreferrer">@NorthNorfolkDC</a></td>
<td><a href="https://www.facebook.com/NorthNorfolkDC" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.n-somerset.gov.uk/" rel="noopener noreferrer">North Somerset Council</a></strong></td>
<td><a href="https://twitter.com/NorthSomersetC" rel="noopener noreferrer">@NorthSomersetC</a></td>
<td><a href="https://www.facebook.com/northsomersetcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td><a href="https://www.linkedin.com/company/35187" rel="noopener noreferrer">LinkedIn</a></td>
</tr><tr><td><strong><a href="http://www.northtyneside.gov.uk/" rel="noopener noreferrer">North Tyneside Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/NTCouncilTeam" rel="noopener noreferrer">@NTCouncilTeam</a></td>
<td><a href="https://www.facebook.com/NTCouncilTeam" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.northwarks.gov.uk/" rel="noopener noreferrer">North Warwickshire Borough Council</a></strong></td>
<td><a href="https://twitter.com/North_Warks_BC" rel="noopener noreferrer">@North_Warks_BC</a></td>
<td><a href="https://www.facebook.com/northwarksbc" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.nwleics.gov.uk/" rel="noopener noreferrer">North West Leicestershire District Council</a></strong></td>
<td><a href="https://twitter.com/NWLeics" rel="noopener noreferrer">@NWLeics</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.northyorks.gov.uk/" rel="noopener noreferrer">North Yorkshire County Council</a></strong></td>
<td><a href="https://twitter.com/northyorkscc" rel="noopener noreferrer">@northyorkscc</a></td>
<td> </td>
<td> </td>
<td> </td>
<td><a href="https://www.flickr.com/photos/northyorkscc" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.northampton.gov.uk/" rel="noopener noreferrer">Northampton Borough Council</a></strong></td>
<td><a href="https://twitter.com/northamptonbc" rel="noopener noreferrer">@northamptonbc</a></td>
<td><a href="https://www.facebook.com/NorthamptonBC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/northamptonbcTV" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.northamptonshire.gov.uk/" rel="noopener noreferrer">Northamptonshire County Council</a></strong></td>
<td><a href="https://twitter.com/mycountycouncil" rel="noopener noreferrer">@mycountycouncil</a></td>
<td><a href="https://www.facebook.com/mycountycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/nccwebteam" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.linkedin.com/company/northamptonshire-county-council" rel="noopener noreferrer">LinkedIn</a></td>
</tr><tr><td><strong><a href="http://www.northumberland.gov.uk/" rel="noopener noreferrer">Northumberland Council</a></strong></td>
<td><a href="https://twitter.com/n_landcouncil" rel="noopener noreferrer">@n_landcouncil</a></td>
<td><a href="https://www.facebook.com/nccalerts" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/NorthumberlandTV" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.pinterest.com/northumberlandc/" rel="noopener noreferrer">Pinterest</a></td>
</tr><tr><td><strong><a href="http://www.norwich.gov.uk/" rel="noopener noreferrer">Norwich City Council</a></strong></td>
<td><a href="https://twitter.com/NorwichCC" rel="noopener noreferrer">@NorwichCC</a></td>
<td><a href="https://www.facebook.com/NorwichCityCouncilEvents" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td><a href="https://www.flickr.com/photos/norwichcc/sets" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.nottinghamcity.gov.uk/" rel="noopener noreferrer">Nottingham City Council</a></strong></td>
<td><a href="https://twitter.com/mynottingham" rel="noopener noreferrer">@mynottingham</a></td>
<td><a href="https://www.facebook.com/mynottingham" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/NottCityCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.nottinghamshire.gov.uk/" rel="noopener noreferrer">Nottinghamshire County Council</a></strong></td>
<td><a href="https://twitter.com/NottsCC" rel="noopener noreferrer">@NottsCC</a></td>
<td><a href="https://www.facebook.com/nottinghamshire" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.nuneatonandbedworth.gov.uk/" rel="noopener noreferrer">Nuneaton and Bedworth Borough Council</a></strong></td>
<td><a href="https://twitter.com/nbbcouncil" rel="noopener noreferrer">@nbbcouncil</a></td>
<td><a href="https://www.facebook.com/NBBCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://oadby-wigston.gov.uk/" rel="noopener noreferrer">Oadby and Wigston District Council</a></strong></td>
<td><a href="https://twitter.com/Oadby_Wigston" rel="noopener noreferrer">@Oadby_Wigston</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.oldham.gov.uk/" rel="noopener noreferrer">Oldham Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/OldhamCouncil" rel="noopener noreferrer">@OldhamCouncil</a></td>
<td><a href="https://www.facebook.com/loveoldham" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.oxford.gov.uk/" rel="noopener noreferrer">Oxford City Council</a></strong></td>
<td><a href="https://twitter.com/OxfordCity" rel="noopener noreferrer">@OxfordCity</a></td>
<td><a href="https://www.facebook.com/OxfordCityCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/OxfordCityCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.instagram.com/oxfordcitycouncil/" rel="noopener noreferrer">Instagram</a></td>
</tr><tr><td><strong><a href="http://www.oxfordshire.gov.uk/" rel="noopener noreferrer">Oxfordshire County Council</a></strong></td>
<td><a href="https://twitter.com/oxfordshirecc" rel="noopener noreferrer">@oxfordshirecc</a></td>
<td><a href="https://www.facebook.com/OxfordshireCountyCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.pembrokeshire.gov.uk/" rel="noopener noreferrer">Pembrokeshire County Council</a></strong></td>
<td><a href="https://twitter.com/pembrokeshire" rel="noopener noreferrer">@pembrokeshire</a></td>
<td><a href="https://www.facebook.com/PembrokeshireCountyCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/PembsCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.pendle.gov.uk/" rel="noopener noreferrer">Pendle Borough Council</a></strong></td>
<td><a href="https://twitter.com/pendlebc" rel="noopener noreferrer">@pendlebc</a></td>
<td><a href="https://www.facebook.com/pendlecouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/PendleCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.pkc.gov.uk/" rel="noopener noreferrer">Perth and Kinross Council</a></strong></td>
<td><a href="https://twitter.com/perthandkinross" rel="noopener noreferrer">@perthandkinross</a></td>
<td><a href="http://www.pkc.gov.uk/socialmedia" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td><a href="https://www.flickr.com/photos/pkcarchive" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.peterborough.gov.uk/" rel="noopener noreferrer">Peterborough City Council</a></strong></td>
<td><a href="https://twitter.com/peterboroughcc" rel="noopener noreferrer">@peterboroughcc</a></td>
<td><a href="https://www.facebook.com/PeterboroughCC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/PboroughCityCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/peterboroughcc/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.plymouth.gov.uk/" rel="noopener noreferrer">Plymouth City Council</a></strong></td>
<td><a href="https://twitter.com/plymouthcc" rel="noopener noreferrer">@plymouthcc</a></td>
<td><a href="https://www.facebook.com/PlymouthCityCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/plymouthcitycouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/plymoutharchives/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.portsmouth.gov.uk/" rel="noopener noreferrer">Portsmouth City Council</a></strong></td>
<td><a href="https://twitter.com/portsmouthtoday" rel="noopener noreferrer">@portsmouthtoday</a></td>
<td><a href="https://www.facebook.com/Portsmouthcitycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCpdYrJR3aCngwulT4yV00XQ" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/portsmouthcitycouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.powys.gov.uk/" rel="noopener noreferrer">Powys County Council</a></strong></td>
<td><a href="https://twitter.com/powyscc" rel="noopener noreferrer">@powyscc</a></td>
<td><a href="https://www.facebook.com/powyscc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCop_U-YVW7OB0jRIt3b8f1Q" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.preston.gov.uk/" rel="noopener noreferrer">Preston City Council</a></strong></td>
<td><a href="https://twitter.com/prestoncouncil" rel="noopener noreferrer">@prestoncouncil</a></td>
<td><a href="https://www.facebook.com/PrestonCityCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCayjH-b7GysyX6dWVMbb75Q" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/proudprestonian/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.dorsetforyou.com/" rel="noopener noreferrer">Purbeck District Council</a></strong></td>
<td><a href="https://twitter.com/Purbeck_DC" rel="noopener noreferrer">@purbeck_dc</a></td>
<td><a href="https://www.facebook.com/dorsetforyou" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/dorsetforyou" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/dorsetforyou/sets/72157624600306828/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://beta.reading.gov.uk/" rel="noopener noreferrer">Reading Borough Council</a></strong></td>
<td><a href="https://twitter.com/readingcouncil" rel="noopener noreferrer">@readingcouncil</a></td>
<td><a href="https://www.facebook.com/Readingcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCouoI8U-PxooTU1EA7Z_uqQ" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.redcar-cleveland.gov.uk/" rel="noopener noreferrer">Redcar and Cleveland Council</a></strong></td>
<td><a href="https://twitter.com/redcarcleveland" rel="noopener noreferrer">@redcarcleveland</a></td>
<td><a href="https://www.facebook.com/redcarcleveland" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/RCBCvideos" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/redcarcleveland" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.redditchbc.gov.uk/" rel="noopener noreferrer">Redditch Borough Council</a></strong></td>
<td><a href="https://twitter.com/redditchmatters" rel="noopener noreferrer">@redditchmatters</a></td>
<td><a href="https://www.facebook.com/pages/Redditch-Borough-Council/117765673977" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/RedditchBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/52111561@N03" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.reigate-banstead.gov.uk/" rel="noopener noreferrer">Reigate &amp; Banstead Borough Council</a></strong></td>
<td><a href="https://twitter.com/reigatebanstead" rel="noopener noreferrer">@reigatebanstead</a></td>
<td><a href="https://www.facebook.com/ReigateBansteadPolice" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/ReigateBanstead" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/reigate-bansteadboroughcouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.rctcbc.gov.uk/" rel="noopener noreferrer">Rhondda Cynon Taf County Borough Council</a></strong></td>
<td><a href="https://twitter.com/RCTCouncil" rel="noopener noreferrer">@RCTCouncil</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/RCTCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="https://www.ribblevalley.gov.uk/" rel="noopener noreferrer">Ribble Valley Borough Council</a></strong></td>
<td><a href="https://twitter.com/ribblevalleybc" rel="noopener noreferrer">@ribblevalleybc</a></td>
<td><a href="https://www.facebook.com/pages/Arts-Development-Ribble-Valley-Borough-Council/244046278979656" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/ribblevalleygov" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://richmondshire.gov.uk/" rel="noopener noreferrer">Richmondshire District Council</a></strong></td>
<td><a href="https://twitter.com/richmondshiredc" rel="noopener noreferrer">@richmondshiredc</a></td>
<td><a href="https://www.facebook.com/richmondshiredc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UC-uxwJuH1UkDoTN707n86cQ" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.rochdale.gov.uk/" rel="noopener noreferrer">Rochdale Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/rochdalecouncil" rel="noopener noreferrer">@rochdalecouncil</a></td>
<td><a href="https://www.facebook.com/rochdalecouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCLz3H_x6KN68JKtx3xhiJWw" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/rochdalecouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.rochford.gov.uk/" rel="noopener noreferrer">Rochford District Council</a></strong></td>
<td><a href="https://twitter.com/rochforddc" rel="noopener noreferrer">@rochforddc</a></td>
<td><a href="https://www.facebook.com/pages/Recycle-For-Rochford/276358509073126" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/rochfordcouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/rochfordcouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.rossendale.gov.uk/" rel="noopener noreferrer">Rossendale Borough Council</a></strong></td>
<td><a href="https://twitter.com/rossendalebc" rel="noopener noreferrer">@rossendalebc</a></td>
<td><a href="https://www.facebook.com/RossendaleCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/RossendaleCouncil74" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/lancashirecc/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.rother.gov.uk/" rel="noopener noreferrer">Rother District Council</a></strong></td>
<td><a href="https://twitter.com/rotherdc" rel="noopener noreferrer">@rotherdc</a></td>
<td><a href="https://www.facebook.com/RotherDC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/RotherDC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/rotherdc/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.rotherham.gov.uk/" rel="noopener noreferrer">Rotherham Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/rmbcpress" rel="noopener noreferrer">@rmbcpress</a></td>
<td><a href="https://www.facebook.com/RMBCCultureLeisure" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/RotherhamMBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.royalgreenwich.gov.uk/" rel="noopener noreferrer">Royal Borough of Greenwich</a></strong></td>
<td><a href="https://twitter.com/royal_greenwich" rel="noopener noreferrer">@royal_greenwich</a></td>
<td><a href="https://www.facebook.com/royalgreenwich" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/royalgreenwich" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://www.royalgreenwich.gov.uk/blog/mayor" rel="noopener noreferrer">Mayor's blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.rbkc.gov.uk/" rel="noopener noreferrer">Royal Borough of Kensington and Chelsea</a></strong></td>
<td><a href="https://twitter.com/rbkc" rel="noopener noreferrer">@rbkc</a></td>
<td><a href="https://www.facebook.com/RoyalBorough" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/kensingtonandchelsea" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.kingston.gov.uk/" rel="noopener noreferrer">Royal Borough of Kingston upon Thames</a></strong></td>
<td><a href="https://twitter.com/RBKingston" rel="noopener noreferrer">@RBKingston</a></td>
<td><a href="https://www.facebook.com/pages/Kingston-Council/296821933791980" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/KingstonCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/kingston-gov-uk/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.rbwm.gov.uk/" rel="noopener noreferrer">Royal Borough of Windsor and Maidenhead</a></strong></td>
<td><a href="https://twitter.com/rbwm" rel="noopener noreferrer">@rbwm</a></td>
<td><a href="https://www.facebook.com/pages/RBWM/118508024841691" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/WindsorMaidenhead" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.rugby.gov.uk/" rel="noopener noreferrer">Rugby Borough Council</a></strong></td>
<td><a href="https://twitter.com/rugbybc" rel="noopener noreferrer">@rugbybc</a></td>
<td><a href="https://www.facebook.com/RugbyCommunities" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/RugbyBoroughCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/rugbybc" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.runnymede.gov.uk/" rel="noopener noreferrer">Runnymede Borough Council</a></strong></td>
<td><a href="https://twitter.com/runnymedebc" rel="noopener noreferrer">@runnymedebc</a></td>
<td><a href="https://www.facebook.com/therunnymede?rf=139421309435628" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/rbcplanning" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.rushcliffe.gov.uk/" rel="noopener noreferrer">Rushcliffe Borough Council</a></strong></td>
<td><a href="https://twitter.com/rushcliffe" rel="noopener noreferrer">@rushcliffe</a></td>
<td><a href="https://www.facebook.com/rushcliffe" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/RushcliffeBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.pinterest.com/rushcliffe/" rel="noopener noreferrer">Pinterest</a></td>
</tr><tr><td><strong><a href="http://www.rushmoor.gov.uk/" rel="noopener noreferrer">Rushmoor Borough Council</a></strong></td>
<td><a href="https://twitter.com/rushmoorcouncil" rel="noopener noreferrer">@rushmoorcouncil</a></td>
<td><a href="https://www.facebook.com/rushmoorboroughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/RushmoorCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/rushmoorcouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.rutland.gov.uk/" rel="noopener noreferrer">Rutland County Council</a></strong></td>
<td><a href="https://twitter.com/rutlandcouncil" rel="noopener noreferrer">@rutlandcouncil</a></td>
<td><a href="https://www.facebook.com/pages/Rutland-County-Council/127845820621640" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td><a href="https://www.flickr.com/photos/rutlandcouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.ryedale.gov.uk/" rel="noopener noreferrer">Ryedale District Council</a></strong></td>
<td><a href="https://twitter.com/ryedaledc" rel="noopener noreferrer">@ryedaledc</a></td>
<td><a href="https://www.facebook.com/RyedaleDC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/ryedaledc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/ryedaledc" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.salford.gov.uk/" rel="noopener noreferrer">Salford City Council</a></strong></td>
<td><a href="https://twitter.com/salfordcouncil" rel="noopener noreferrer">@salfordcouncil</a></td>
<td><a href="https://www.facebook.com/SalfordCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SalfordCityCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/salfordcitycouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.sandwell.gov.uk/" rel="noopener noreferrer">Sandwell Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/sandwellcouncil" rel="noopener noreferrer">@sandwellcouncil</a></td>
<td><a href="https://www.facebook.com/sandwellcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/sandwellchannel" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.scarborough.gov.uk/" rel="noopener noreferrer">Scarborough Borough Council</a></strong></td>
<td><a href="https://twitter.com/scarborocouncil" rel="noopener noreferrer">@scarborocouncil</a></td>
<td><a href="https://www.facebook.com/scarboroughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.sedgemoor.gov.uk/" rel="noopener noreferrer">Sedgemoor District Council</a></strong></td>
<td><a href="https://twitter.com/sedgemoordc" rel="noopener noreferrer">@sedgemoordc</a></td>
<td><a href="https://www.facebook.com/pages/Sedgemoor-District-Council/307799872701081?rf=426840694025459" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SedgemoorDC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.sefton.gov.uk/" rel="noopener noreferrer">Sefton Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/seftoncouncil" rel="noopener noreferrer">@seftoncouncil</a></td>
<td><a href="https://www.facebook.com/pages/Sefton-Council/634089093354185" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SeftonCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.selby.gov.uk/" rel="noopener noreferrer">Selby District Council</a></strong></td>
<td><a href="https://twitter.com/selbydc" rel="noopener noreferrer">@selbydc</a></td>
<td><a href="https://www.facebook.com/SelbyDC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/selbycefs" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.sevenoaks.gov.uk/" rel="noopener noreferrer">Sevenoaks District Council</a></strong></td>
<td><a href="https://twitter.com/SDC_newsdesk" rel="noopener noreferrer">@SDC_newsdesk</a></td>
<td><a href="https://www.facebook.com/sevenoaksdc" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td><a href="https://www.pinterest.com/sdcouncil/" rel="noopener noreferrer">Pinterest</a></td>
</tr><tr><td><strong><a href="http://www.sheffield.gov.uk/" rel="noopener noreferrer">Sheffield City Council</a></strong></td>
<td><a href="https://twitter.com/sheffcouncil" rel="noopener noreferrer">@sheffcouncil</a></td>
<td><a href="https://www.facebook.com/sheffgovuk" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SheffieldCCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://www.picturesheffield.com/" rel="noopener noreferrer">Picture Sheffield</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.shepway.gov.uk/" rel="noopener noreferrer">Shepway District Council</a></strong></td>
<td><a href="https://twitter.com/shepwaydc" rel="noopener noreferrer">@shepwaydc</a></td>
<td><a href="https://www.facebook.com/ShepwayDistrictCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/ShepwayDC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.shropshire.gov.uk/" rel="noopener noreferrer">Shropshire Council - Unitary</a></strong></td>
<td><a href="https://twitter.com/ShropCouncil" rel="noopener noreferrer">@ShropCouncil</a></td>
<td><a href="https://www.facebook.com/ShropshireArchives" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/ShropshireCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/shropshirecouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.slough.gov.uk/" rel="noopener noreferrer">Slough Borough Council</a></strong></td>
<td><a href="http://www.twitter.com/sloughcouncil" rel="noopener noreferrer">@sloughcouncil</a></td>
<td><a href="https://en-gb.facebook.com/slough.boroughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SloughBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.solihull.gov.uk/" rel="noopener noreferrer">Solihull Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/solihullcouncil" rel="noopener noreferrer">@solihullcouncil</a></td>
<td><a href="https://www.facebook.com/solihullcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.somerset.gov.uk/" rel="noopener noreferrer">Somerset County Council</a></strong></td>
<td><a href="https://twitter.com/SomersetCouncil" rel="noopener noreferrer">@SomersetCouncil</a></td>
<td><a href="https://www.facebook.com/somersetcountycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCn1IsPZUJBPIZC5wBVoBA_w" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.linkedin.com/company/somerset-county-council" rel="noopener noreferrer">LinkedIn</a><br /><a href="http://www.flickr.com/somersetcountycouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.southbucks.gov.uk/" rel="noopener noreferrer">South Buckinghamshire District Council</a></strong></td>
<td><a href="https://twitter.com/SouthBucksDC" rel="noopener noreferrer">@SouthBucksDC</a></td>
<td><a href="https://www.facebook.com/pages/South-Bucks-District-Council/421923837822113" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.scambs.gov.uk/" rel="noopener noreferrer">South Cambridgeshire District Council</a></strong></td>
<td><a href="https://twitter.com/SouthCambs" rel="noopener noreferrer">@SouthCambs</a></td>
<td><a href="https://www.facebook.com/pages/South-Cambridgeshire/153049928086525" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SouthCambsDC/" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.south-derbys.gov.uk/" rel="noopener noreferrer">South Derbyshire District Council</a></strong></td>
<td><a href="https://twitter.com/sddc" rel="noopener noreferrer">@sddc</a></td>
<td><a href="https://www.facebook.com/southderbys" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.southglos.gov.uk/" rel="noopener noreferrer">South Gloucestershire Council</a></strong></td>
<td><a href="https://twitter.com/sgloscouncil" rel="noopener noreferrer">@sgloscouncil</a></td>
<td><a href="https://www.facebook.com/sgloscouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/southgloscouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.southhams.gov.uk/" rel="noopener noreferrer">South Hams District Council</a></strong></td>
<td><a href="https://twitter.com/@SouthHams_DC" rel="noopener noreferrer">@SouthHams_DC</a></td>
<td><a href="https://www.facebook.com/southhamsdistrictcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SouthHamsCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.sholland.gov.uk/" rel="noopener noreferrer">South Holland District Council</a></strong></td>
<td><a href="https://twitter.com/SHollandDC" rel="noopener noreferrer">@SHollandDC</a></td>
<td><a href="https://www.facebook.com/SouthHollandDistrictCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SouthHollandDC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.southkesteven.gov.uk/" rel="noopener noreferrer">South Kesteven District Council</a></strong></td>
<td><a href="https://twitter.com/southkesteven" rel="noopener noreferrer">@southkesteven</a></td>
<td><a href="https://www.facebook.com/southkdc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SouthKestevenDC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.southlakeland.gov.uk/" rel="noopener noreferrer">South Lakeland District Council</a></strong></td>
<td><a href="https://twitter.com/southlakelanddc" rel="noopener noreferrer">@southlakelanddc</a></td>
<td><a href="https://www.facebook.com/southlakelanddistrictcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.south-norfolk.gov.uk/" rel="noopener noreferrer">South Norfolk District Council</a></strong></td>
<td><a href="https://twitter.com/snorfolkcouncil" rel="noopener noreferrer">@snorfolkcouncil</a></td>
<td><a href="https://www.facebook.com/southnorfolkcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.southnorthants.gov.uk/" rel="noopener noreferrer">South Northamptonshire Council</a></strong></td>
<td><a href="https://twitter.com/SNorthantsC" rel="noopener noreferrer">@SNorthantsC</a></td>
<td><a href="https://www.facebook.com/SouthNorthantsCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SNC01327322322" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.southoxon.gov.uk/" rel="noopener noreferrer">South Oxfordshire District Council</a></strong></td>
<td><a href="https://twitter.com/southoxon" rel="noopener noreferrer">@southoxon</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/SouthandVale" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.southribble.gov.uk/" rel="noopener noreferrer">South Ribble Borough Council</a></strong></td>
<td><a href="https://twitter.com/southribblebc" rel="noopener noreferrer">@southribblebc</a></td>
<td><a href="https://www.facebook.com/mysouthribble" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCvbFEXJ9yRwXJHysfbkQ7Kw" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="///C:\Users\Alla.Taha\AppData\Local\temp\www.southsomerset.gov.uk" rel="noopener noreferrer">South Somerset District Council</a></strong></td>
<td><a href="https://twitter.com/southsomersetdc" rel="noopener noreferrer">@southsomersetdc</a></td>
<td><a href="https://www.facebook.com/SouthSomersetDistrictCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/southsomersetdc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.sstaffs.gov.uk/" rel="noopener noreferrer">South Staffordshire Council</a></strong></td>
<td><a href="https://twitter.com/south_staffs" rel="noopener noreferrer">@south_staffs</a></td>
<td><a href="https://www.facebook.com/yourstaffordshire" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/southstaffordshire" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.southtyneside.info/" rel="noopener noreferrer">South Tyneside Council</a></strong></td>
<td><a href="https://twitter.com/STynesideEvents" rel="noopener noreferrer">@STynesideEvents</a></td>
<td><a href="https://www.facebook.com/SouthTynesideEvents" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SouthTynesideCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.southampton.gov.uk/" rel="noopener noreferrer">Southampton City Council</a></strong></td>
<td><a href="https://twitter.com/southamptoncc" rel="noopener noreferrer">@southamptoncc</a></td>
<td><a href="https://www.facebook.com/SotonCC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SouthamptonNews" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.southend.gov.uk/" rel="noopener noreferrer">Southend-on-Sea Borough Council</a></strong></td>
<td><a href="https://twitter.com/SouthendBC" rel="noopener noreferrer">@SouthendBC</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.spelthorne.gov.uk/" rel="noopener noreferrer">Spelthorne Borough Council</a></strong></td>
<td><a href="https://twitter.com/SpelthorneBC" rel="noopener noreferrer">@SpelthorneBC</a></td>
<td><a href="https://www.facebook.com/spelthorne.council" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.stalbans.gov.uk/" rel="noopener noreferrer">St Albans City and District Council</a></strong></td>
<td><a href="https://twitter.com/StAlbansCouncil" rel="noopener noreferrer">@StAlbansCouncil</a></td>
<td> </td>
<td><a href="https://www.youtube.com/channel/UCY2158WuxEVXohDhxV-zZZQ" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.stedmundsbury.gov.uk/" rel="noopener noreferrer">St Edmundsbury Borough Council</a></strong></td>
<td><a href="https://twitter.com/stedsbc" rel="noopener noreferrer">@stedsbc</a></td>
<td><a href="https://www.facebook.com/StEdmundsburynews" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.sthelens.gov.uk/" rel="noopener noreferrer">St Helens Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/sthelenscouncil" rel="noopener noreferrer">@sthelenscouncil</a></td>
<td><a href="https://www.facebook.com/sthelenscouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/sthelenscouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.staffordbc.gov.uk/" rel="noopener noreferrer">Stafford Borough Council</a></strong></td>
<td><a href="https://twitter.com/Staffordbc" rel="noopener noreferrer">@Staffordbc</a></td>
<td><a href="https://en-gb.facebook.com/staffordbc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UC3IXmcDtYV8AMo7QxhQ996Q" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.staffordshire.gov.uk/" rel="noopener noreferrer">Staffordshire County Council</a></strong></td>
<td><a href="https://twitter.com/StaffordshireCC" rel="noopener noreferrer">@StaffordshireCC</a></td>
<td><a href="https://www.facebook.com/yourstaffordshire" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/StaffsCC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.staffsmoorlands.gov.uk/" rel="noopener noreferrer">Staffordshire Moorlands District Council</a></strong></td>
<td><a href="https://twitter.com/TweetSMDC" rel="noopener noreferrer">@TweetSMDC</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/staffsmoorlandsdc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.stevenage.gov.uk/" rel="noopener noreferrer">Stevenage Borough Council</a></strong></td>
<td><a href="https://twitter.com/stevenagebc" rel="noopener noreferrer">@stevenagebc</a></td>
<td><a href="https://www.facebook.com/stevenageboroughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SBCComms" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.stockport.gov.uk/" rel="noopener noreferrer">Stockport Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/StockportMBC" rel="noopener noreferrer">@StockportMBC</a></td>
<td><a href="https://www.facebook.com/StockportMBC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/StockportCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.stockton.gov.uk/" rel="noopener noreferrer">Stockton-on-Tees Borough Council</a></strong></td>
<td><a href="https://twitter.com/stocktoncouncil" rel="noopener noreferrer">@stocktoncouncil</a></td>
<td><a href="https://www.facebook.com/stocktoncouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/Stocktoncouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.stoke.gov.uk/" rel="noopener noreferrer">Stoke-on-Trent City Council</a></strong></td>
<td><a href="https://twitter.com/sotcitycouncil" rel="noopener noreferrer">@sotcitycouncil</a></td>
<td><a href="https://www.facebook.com/sotcitycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/StokeOnTrentCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.strabanedc.org.uk/" rel="noopener noreferrer">Strabane District Council</a></strong></td>
<td><a href="https://twitter.com/dcsdcouncil" rel="noopener noreferrer">@dcsdcouncil</a></td>
<td><a href="https://www.facebook.com/StrabaneDistrictCouncil/timeline" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SDCTVStrabane" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.stratford.gov.uk/" rel="noopener noreferrer">Stratford-on-Avon District Council</a></strong></td>
<td><a href="https://twitter.com/StratfordDC" rel="noopener noreferrer">@StratfordDC</a></td>
<td><a href="https://www.facebook.com/StratfordDC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/StratfordDC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.stroud.gov.uk/" rel="noopener noreferrer">Stroud District Council</a></strong></td>
<td><a href="https://twitter.com/strouddc" rel="noopener noreferrer">@strouddc</a></td>
<td><a href="https://www.facebook.com/strouddc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCHVHSJkEdtb-HemHPRVAXDQ" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.suffolkcoastal.gov.uk/" rel="noopener noreferrer">Suffolk Coastal District Council</a></strong></td>
<td><a href="https://twitter.com/suffolkcoastal" rel="noopener noreferrer">@suffolkcoastal</a></td>
<td><a href="https://www.facebook.com/pages/Suffolk-Coastal-District-Council/105380112834385" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.suffolk.gov.uk/" rel="noopener noreferrer">Suffolk County Council</a></strong></td>
<td><a href="https://twitter.com/suffolkcc" rel="noopener noreferrer">@suffolkcc</a></td>
<td><a href="https://www.facebook.com/pages/Suffolk-County-Council/158525844191005" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCCU85--u-eTpf0bvY83inDw" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.sunderland.gov.uk/" rel="noopener noreferrer">Sunderland City Council</a></strong></td>
<td><a href="https://twitter.com/sunderlanduk" rel="noopener noreferrer">@sunderlanduk</a></td>
<td><a href="https://www.facebook.com/Sunderland.TheCity" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/sunderlandgov" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://www.sunderland.gov.uk/index.aspx?articleid=2108" rel="noopener noreferrer">Leader's blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.surreycc.gov.uk/" rel="noopener noreferrer">Surrey County Council</a></strong></td>
<td><a href="https://twitter.com/SurreyCouncil" rel="noopener noreferrer">@SurreyCouncil</a></td>
<td><a href="https://www.facebook.com/surreymatters" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SurreyCountyCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.surreyheath.gov.uk/" rel="noopener noreferrer">Surrey Heath Borough Council</a></strong></td>
<td><a href="https://twitter.com/surreyheath" rel="noopener noreferrer">@surreyheath</a></td>
<td><a href="https://www.facebook.com/SurreyHeath" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/SurreyHeathBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.swale.gov.uk/" rel="noopener noreferrer">Swale Borough Council</a></strong></td>
<td><a href="https://twitter.com/SwaleCouncil" rel="noopener noreferrer">@SwaleCouncil</a></td>
<td><a href="https://www.facebook.com/pages/Swale-Borough-Council/148774685144500" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.swansea.gov.uk/" rel="noopener noreferrer">Swansea City and Borough Council</a></strong></td>
<td><a href="https://twitter.com/swanseacouncil" rel="noopener noreferrer">@swanseacouncil</a></td>
<td><a href="https://www.facebook.com/swanseacitycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/swanseacitycouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td>.</td>
<td> </td>
</tr><tr><td><strong><a href="http://www.swindon.gov.uk/" rel="noopener noreferrer">Swindon Borough Council</a></strong></td>
<td><a href="https://twitter.com/swindonnews" rel="noopener noreferrer">@swindonnews</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/SwindonCouncil/videos" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.tameside.gov.uk/" rel="noopener noreferrer">Tameside Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/TamesideCouncil" rel="noopener noreferrer">@TamesideCouncil</a></td>
<td><a href="https://www.facebook.com/pages/Tameside-Council/55100423375" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/TamesideMBC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.tamworth.gov.uk/" rel="noopener noreferrer">Tamworth Borough Council</a></strong></td>
<td><a href="https://twitter.com/tamworthcouncil" rel="noopener noreferrer">@tamworthcouncil</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/TamworthCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="https://tamworthcouncil.wordpress.com/" rel="noopener noreferrer">Tamworth Council's blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.tandridge.gov.uk/" rel="noopener noreferrer">Tandridge District Council</a></strong></td>
<td><a href="https://twitter.com/TandridgeDC" rel="noopener noreferrer">@TandridgeDC</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.tauntondeane.gov.uk/" rel="noopener noreferrer">Taunton Deane Borough Council</a></strong></td>
<td><a href="https://twitter.com/TDBC" rel="noopener noreferrer">@TDBC</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.teignbridge.gov.uk/" rel="noopener noreferrer">Teignbridge District Council</a></strong></td>
<td><a href="https://twitter.com/Teignbridge" rel="noopener noreferrer">@Teignbridge</a></td>
<td><a href="https://www.facebook.com/Teignbridge" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/TeignbridgeDC" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.telford.gov.uk/" rel="noopener noreferrer">Telford &amp; Wrekin Council</a></strong></td>
<td><a href="https://twitter.com/telfordwrekin" rel="noopener noreferrer">@telfordwrekin</a></td>
<td><a href="https://www.facebook.com/TelfordWrekin" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td><a href="http://www.telford.gov.uk/blog/leadersblog" rel="noopener noreferrer">The Leader's Blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.tendringdc.gov.uk/" rel="noopener noreferrer">Tendring District Council</a></strong></td>
<td><a href="https://twitter.com/Tendring_DC" rel="noopener noreferrer">@Tendring_DC</a></td>
<td> </td>
<td><a href="https://www.youtube.com/channel/UC8d4Nph4wRieUrls0AWDnpQ" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.testvalley.gov.uk/" rel="noopener noreferrer">Test Valley Borough Council</a></strong></td>
<td><a href="https://twitter.com/TestValleyBC" rel="noopener noreferrer">@TestValleyBC</a></td>
<td><a href="https://www.facebook.com/TestValleyBC" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://tewkesbury.gov.uk/" rel="noopener noreferrer">Tewkesbury Borough Council</a></strong></td>
<td><a href="https://twitter.com/TewkesburyBCgov" rel="noopener noreferrer">@TewkesburyBCgov</a></td>
<td><a href="https://www.facebook.com/tewkesbury.boroughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://thanet.gov.uk/" rel="noopener noreferrer">Thanet District Council</a></strong></td>
<td><a href="https://twitter.com/thanetcouncil" rel="noopener noreferrer">@thanetcouncil</a></td>
<td><a href="https://www.facebook.com/pages/Thanet-District-Council/83700846372" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/ThanetCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.threerivers.gov.uk/" rel="noopener noreferrer">Three Rivers District Council</a></strong></td>
<td><a href="https://twitter.com/threeriversdc" rel="noopener noreferrer">@threeriversdc</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/TRDCcomms" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.thurrock.gov.uk/" rel="noopener noreferrer">Thurrock Council</a></strong></td>
<td><a href="https://twitter.com/thurrockcouncil" rel="noopener noreferrer">@thurrockcouncil</a></td>
<td><a href="https://www.facebook.com/thurrockcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/thurrockcouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/thurrockcouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.tmbc.gov.uk/" rel="noopener noreferrer">Tonbridge and Malling Borough Council</a></strong></td>
<td><a href="https://twitter.com/tmbc_kent" rel="noopener noreferrer">@tmbc_kent</a></td>
<td> </td>
<td><a href="https://www.youtube.com/channel/UCylv4iItmY5-NSJV5UiuUAQ" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.torbay.gov.uk/" rel="noopener noreferrer">Torbay Council</a></strong></td>
<td><a href="https://twitter.com/Torbay_Council" rel="noopener noreferrer">@Torbay_Council</a></td>
<td><a href="https://www.facebook.com/torbaycouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/torbaycouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/torbaycouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.torfaen.gov.uk/" rel="noopener noreferrer">Torfaen County Borough Council</a></strong></td>
<td><a href="https://twitter.com/torfaencouncil" rel="noopener noreferrer">@torfaencouncil</a></td>
<td><a href="https://www.facebook.com/torfaen" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/torfaencouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.torridge.gov.uk/" rel="noopener noreferrer">Torridge District Council</a></strong></td>
<td><a href="https://twitter.com/torridgedc" rel="noopener noreferrer">@torridgedc</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/torridgedc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.trafford.gov.uk/" rel="noopener noreferrer">Trafford Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/traffordcouncil" rel="noopener noreferrer">@traffordcouncil</a></td>
<td><a href="https://www.facebook.com/traffordcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/traffordcouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/traffordcouncil/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.tunbridgewells.gov.uk/" rel="noopener noreferrer">Tunbridge Wells Borough Council</a></strong></td>
<td><a href="https://twitter.com/TWellsCouncil" rel="noopener noreferrer">@TWellsCouncil</a></td>
<td><a href="https://www.facebook.com/pages/Tunbridge-Wells-Borough-Council" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.uttlesford.gov.uk/" rel="noopener noreferrer">Uttlesford District Council</a></strong></td>
<td><a href="https://twitter.com/UttlesfordDC" rel="noopener noreferrer">@UttlesfordDC</a></td>
<td><a href="https://www.facebook.com/UttlesfordDC" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.valeofglamorgan.gov.uk/" rel="noopener noreferrer">Vale of Glamorgan Council</a></strong></td>
<td><a href="https://twitter.com/VOGCouncil" rel="noopener noreferrer">@VOGCouncil</a></td>
<td><a href="https://www.facebook.com/valeofglamorganlife" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/VOGcouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.whitehorsedc.gov.uk/" rel="noopener noreferrer">Vale of White Horse District Council</a></strong></td>
<td><a href="https://twitter.com/whitehorsedc" rel="noopener noreferrer">@whitehorsedc</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.wakefield.gov.uk/" rel="noopener noreferrer">Wakefield Metropolitan District Council</a></strong></td>
<td><a href="https://twitter.com/mywakefield" rel="noopener noreferrer">@mywakefield</a></td>
<td><a href="https://www.facebook.com/mywakefield" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/WakefieldCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://vimeo.com/wakefieldcouncil" rel="noopener noreferrer">Vimeo</a></td>
</tr><tr><td><strong><a href="http://www.walsall.gov.uk/" rel="noopener noreferrer">Walsall Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/WalsallCouncil" rel="noopener noreferrer">@WalsallCouncil</a></td>
<td><a href="https://www.facebook.com/pages/Our-Walsall/281925178567" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/results?search_query=walsall+council&amp;aq=f" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="http://www.flickr.com/groups/366117@N24/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.warrington.gov.uk/" rel="noopener noreferrer">Warrington Borough Council</a></strong></td>
<td><a href="https://twitter.com/warringtonbc" rel="noopener noreferrer">@warringtonbc</a></td>
<td><a href="https://www.facebook.com/warringtonbc" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/WarringtonBC" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://www.warrington.gov.uk/blog/strong" rel="noopener noreferrer">Growing a Strong Warrington blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.warwickdc.gov.uk/" rel="noopener noreferrer">Warwick District Council</a></strong></td>
<td><a href="https://twitter.com/warwick_dc" rel="noopener noreferrer">@warwick_dc</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/warwickdc07" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.warwickshire.gov.uk/" rel="noopener noreferrer">Warwickshire County Council</a></strong></td>
<td><a href="https://twitter.com/warksdirect" rel="noopener noreferrer">@warksdirect</a></td>
<td> </td>
<td><a href="https://www.youtube.com/warwickshirecc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.watford.gov.uk/" rel="noopener noreferrer">Watford Borough Council</a></strong></td>
<td><a href="https://twitter.com/WatfordCouncil" rel="noopener noreferrer">@WatfordCouncil</a></td>
<td><a href="https://www.facebook.com/lovewatford" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="http://www.youtube.com/watfordcouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.waveney.gov.uk/" rel="noopener noreferrer">Waveney District Council</a></strong></td>
<td><a href="https://twitter.com/waveneydc" rel="noopener noreferrer">@waveneydc</a></td>
<td><a href="https://www.facebook.com/waveneydc" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.waverley.gov.uk/" rel="noopener noreferrer">Waverley Borough Council</a></strong></td>
<td><a href="https://twitter.com/WaverleyBC" rel="noopener noreferrer">@WaverleyBC</a></td>
<td><a href="https://www.facebook.com/WaverleyBC" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCqjRroo-66fGUICwNRpVk6A" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.wealden.gov.uk/" rel="noopener noreferrer">Wealden District Council</a></strong></td>
<td><a href="https://twitter.com/wealdendistrict" rel="noopener noreferrer">@wealdendistrict</a></td>
<td><a href="https://www.facebook.com/wealden" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.wellingborough.gov.uk/" rel="noopener noreferrer">Wellingborough Borough Council</a></strong></td>
<td><a href="https://twitter.com/BCWboro" rel="noopener noreferrer">@BCWboro</a></td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.welhat.gov.uk/" rel="noopener noreferrer">Welwyn Hatfield Council</a></strong></td>
<td><a href="https://twitter.com/welhatcouncil" rel="noopener noreferrer">@welhatcouncil</a></td>
<td><a href="https://www.facebook.com/welwynhatfield" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCNdgihpZ9I_5r36ewx6EkHg" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.westberks.gov.uk/" rel="noopener noreferrer">West Berkshire Council</a></strong></td>
<td><a href="https://twitter.com/westberkshire" rel="noopener noreferrer">@westberkshire</a></td>
<td><a href="https://www.facebook.com/westberkshire" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCPkB2WQ879x052W2Qb5_1jw" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://info.westberks.gov.uk/index.aspx?articleid=26988" rel="noopener noreferrer">West Berkshire blogs</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.westdevon.gov.uk/" rel="noopener noreferrer">West Devon Borough Council</a></strong></td>
<td><a href="https://twitter.com/WestDevon_BC" rel="noopener noreferrer">@WestDevon_BC</a></td>
<td><a href="https://www.facebook.com/westdevonboroughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCDZEXV47SxBpXUDR6qbhtiA" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.dorsetforyou.com/" rel="noopener noreferrer">West Dorset District Council</a></strong></td>
<td><a href="https://twitter.com/dorsetforyou" rel="noopener noreferrer">@dorsetforyou</a></td>
<td><a href="https://www.facebook.com/dorsetforyou" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/dorsetforyou" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://news.dorsetforyou.com/blogs/" rel="noopener noreferrer">Dorset newsroom</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.westlancs.gov.uk/" rel="noopener noreferrer">West Lancashire Borough Council</a></strong></td>
<td><a href="https://twitter.com/Westlancsbc" rel="noopener noreferrer">@Westlancsbc</a></td>
<td> </td>
<td><a href="https://www.youtube.com/channel/UCssXste96OHYqI7BrYTaeeg" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.west-lindsey.gov.uk/" rel="noopener noreferrer">West Lindsey District Council</a></strong></td>
<td><a href="https://twitter.com/WestLindseyDC" rel="noopener noreferrer">@WestLindseyDC</a></td>
<td><a href="https://www.facebook.com/westlindseydistrictcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.westoxon.gov.uk/" rel="noopener noreferrer">West Oxfordshire District Council</a></strong></td>
<td><a href="https://twitter.com/WodcNews" rel="noopener noreferrer">@WodcNews</a></td>
<td><a href="https://www.facebook.com/westoxfordshire" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.westsomersetonline.gov.uk/" rel="noopener noreferrer">West Somerset District Council</a></strong></td>
<td><a href="https://twitter.com/wsomerset" rel="noopener noreferrer">@wsomerset</a></td>
<td><a href="https://www.facebook.com/westsomerset" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.westsussex.gov.uk/" rel="noopener noreferrer">West Sussex County Council</a></strong></td>
<td><a href="https://twitter.com/wsccnews" rel="noopener noreferrer">@wsccnews</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/wsccvideo/featured" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.westminster.gov.uk/" rel="noopener noreferrer">Westminster City Council</a></strong></td>
<td><a href="https://twitter.com/CityWestminster" rel="noopener noreferrer">@CityWestminster</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/Westminstercc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="https://www.dorsetforyou.com/" rel="noopener noreferrer">Weymouth and Portland Borough Council</a></strong></td>
<td><a href="https://twitter.com/dorsetforyou" rel="noopener noreferrer">@dorsetforyou</a></td>
<td><a href="https://www.facebook.com/dorsetforyou" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/dorsetforyou" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/dorsetforyou" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.wigan.gov.uk/" rel="noopener noreferrer">Wigan Metropolitan Borough Council</a></strong></td>
<td><a href="https://twitter.com/wigancouncil" rel="noopener noreferrer">@wigancouncil</a></td>
<td><a href="https://www.facebook.com/WiganCouncilOnline" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/wigancouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://instagram.com/wigancouncil/" rel="noopener noreferrer">Instagram</a></td>
</tr><tr><td><strong><a href="http://www.wiltshire.gov.uk/" rel="noopener noreferrer">Wiltshire Council</a></strong></td>
<td><a href="https://twitter.com/wiltscouncil" rel="noopener noreferrer">@wiltscouncil</a></td>
<td><a href="https://www.facebook.com/WiltshireCouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/wiltshirecouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://www.wiltshire.gov.uk/communityandliving/ourcommunitymatterswebsites.htm" rel="noopener noreferrer">Our Communities Matters blogs</a></td>
<td><a href="https://storify.com/WiltsCouncil" rel="noopener noreferrer">Storify</a></td>
</tr><tr><td><strong><a href="http://www.winchester.gov.uk/" rel="noopener noreferrer">Winchester City Council</a></strong></td>
<td><a href="https://twitter.com/winchestercity" rel="noopener noreferrer">@winchestercity</a></td>
<td><a href="https://www.facebook.com/WinchesterCity" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCkm-3iLtlsStVhasHckxG_g" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.pinterest.com/winchestercity/" rel="noopener noreferrer">Pinterest</a></td>
</tr><tr><td><strong><a href="http://www.wirral.gov.uk/" rel="noopener noreferrer">Wirral Council</a></strong></td>
<td><a href="https://twitter.com/WirralCouncil" rel="noopener noreferrer">@WirralCouncil</a></td>
<td><a href="https://www.facebook.com/wirralcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.woking.gov.uk/" rel="noopener noreferrer">Woking Borough Council</a></strong></td>
<td><a href="https://twitter.com/wokingcouncil" rel="noopener noreferrer">@wokingcouncil</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/WokingBoroughCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/99332245@N02/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.wokingham.gov.uk/" rel="noopener noreferrer">Wokingham Borough Council</a></strong></td>
<td><a href="https://twitter.com/WokinghamBC" rel="noopener noreferrer">@WokinghamBC</a></td>
<td><a href="https://www.facebook.com/wokinghamboroughcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/channel/UCVJ4X4AijnyYaLHSAWIzqtA" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/wokinghambc" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.wolverhampton.gov.uk/" rel="noopener noreferrer">Wolverhampton City Council</a></strong></td>
<td><a href="https://twitter.com/wolvescouncil" rel="noopener noreferrer">@wolvescouncil</a></td>
<td><a href="https://www.facebook.com/WolverhamptonToday" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/WolverhamptonToday" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/126301525@N06/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.worcester.gov.uk/" rel="noopener noreferrer">Worcester City Council</a></strong></td>
<td><a href="https://twitter.com/myworcester" rel="noopener noreferrer">@myworcester</a></td>
<td><a href="https://www.facebook.com/worcester" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/WorcesterCityCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td> </td>
</tr><tr><td><strong><a href="http://www.worcestershire.gov.uk/" rel="noopener noreferrer">Worcestershire County Council</a></strong></td>
<td><a href="https://twitter.com/worcscc" rel="noopener noreferrer">@worcscc</a></td>
<td><a href="https://www.facebook.com/YourWorcestershire" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/worcscc" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.linkedin.com/company/worcestershire-county-council/" rel="noopener noreferrer">LinkedIn</a></td>
</tr><tr><td><strong><a href="http://www.wrexham.gov.uk/" rel="noopener noreferrer">Wrexham County Borough Council</a></strong></td>
<td><a href="https://twitter.com/wrexhamcbc" rel="noopener noreferrer">@wrexhamcbc</a></td>
<td><a href="https://www.facebook.com/wrexhamcouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/wrexhamcouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/wrexhamcouncil" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.wychavon.gov.uk/" rel="noopener noreferrer">Wychavon District Council</a></strong></td>
<td><a href="https://twitter.com/Wychavon" rel="noopener noreferrer">@Wychavon</a></td>
<td><a href="https://www.facebook.com/wychavon" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/wychavonvideo" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/wychavon/" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.wycombe.gov.uk/" rel="noopener noreferrer">Wycombe District Council</a></strong></td>
<td><a href="https://twitter.com/wycombedc" rel="noopener noreferrer">@wycombedc</a></td>
<td> </td>
<td><a href="https://www.youtube.com/user/wycombedc" rel="noopener noreferrer">YouTube channel</a></td>
<td><a href="http://www.wycombe.gov.uk/council-services/leisure-and-culture/wycombe-museum/spoken-memories.aspx" rel="noopener noreferrer">Spoken Memories project blog</a></td>
<td> </td>
</tr><tr><td><strong><a href="http://www.wyre.gov.uk/" rel="noopener noreferrer">Wyre Council</a></strong></td>
<td><a href="https://twitter.com/wyrecouncil" rel="noopener noreferrer">@wyrecouncil</a></td>
<td><a href="https://www.facebook.com/wyrecouncil" rel="noopener noreferrer">Facebook page</a></td>
<td><a href="https://www.youtube.com/user/WyreCouncil" rel="noopener noreferrer">YouTube channel</a></td>
<td> </td>
<td><a href="https://www.flickr.com/photos/wyrebc" rel="noopener noreferrer">Flickr</a></td>
</tr><tr><td><strong><a href="http://www.wyreforestdc.gov.uk/" rel="noopener noreferrer">Wyre Forest District Council</a></strong></td>
<td><a href="https://twitter.com/wyreforestdc" rel="noopener noreferrer">@wyreforestdc</a></td>
<td><a href="https://www.facebook.com/WyreForest" rel="noopener noreferrer">Facebook page</a></td>
<td> </td>
<td> </td>
<td> </td>
</tr></tbody></table></div>
COUN;

$results = preg_match_all("/<a href=\"([a-zA-Z_:\/.-]*)\" rel=\"noopener noreferrer\">([a-zA-Z ]*)<\//", $council, $matches);

//print_r($matches);

$urls = $matches[1];
$names = $matches[2];

for($i=0; $i<count($urls); $i++){
    
    if(!strpos($urls[$i], 'instagram')&&!strpos($urls[$i], 'blog')&&!strpos($urls[$i], 'vimeo')&&!strpos($urls[$i], 'linkedin')&&!strpos($urls[$i], 'facebook')&&!strpos($urls[$i], 'flickr') && !strpos($urls[$i], 'youtube')){
        echo $urls[$i].','.str_replace('http://www.', '', $urls[$i]).'<br />';
    }
    
}