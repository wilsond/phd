<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
set_time_limit(0);
$time_start = microtime(true);
//$rustart = getrusage();
// https://stackoverflow.com/questions/535020/tracking-the-script-execution-time-in-php
$start_from = @$_GET['start_from'];
$c = $_GET['c'];
include '_config.php';
include '_global.php';
include '_functions.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crawl a set of sites</title>
    <link rel="stylesheet" type="text/css" href="css/phd.css">

</head>
<body>
<?php menu();?>
<div class="crawl-stats">
<h1>Getting data for <strong><?php echo $c; ?></strong></h1>

<?php

$sql = "SELECT count(*) FROM `" . slug($c) . "-university` ORDER BY id ASC;";

$result = mysqli_query($con, $sql);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $total_count = $row['count(*)'];
    }
}

$sql = "SELECT * FROM `" . slug($c) . "-university` ORDER BY id ASC;";
$database_table = slug($c) . "-university";

$result = mysqli_query($con, $sql);
$count = 1;
if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        if ($row['id'] > $start_from) {
        
            find_viewport($row['site_url'], $row['id'], $start_year, $finish_year, $con, $allowed_distance, $total_count, $count);
            $count++;
            //find_live_viewport($row['site_url'], $row['id'], $con);

        }
    }

} else {
    echo "something has gone wrong";
}

mysqli_close($con);

// function rutime($ru, $rus, $index)
// {
//     return ($ru["ru_$index.tv_sec"] * 1000 + intval($ru["ru_$index.tv_usec"] / 1000))
//          - ($rus["ru_$index.tv_sec"] * 1000 + intval($rus["ru_$index.tv_usec"] / 1000));
// }

// $ru = getrusage();
// echo "This process used " . rutime($ru, $rustart, "utime") .
//     " ms for its computations\n";
// echo "It spent " . rutime($ru, $rustart, "stime") .
//     " ms in system calls\n";

$time_end = microtime(true);

//dividing with 60 will give the execution time in minutes otherwise seconds
$execution_time = ($time_end - $time_start) / 60;

//execution time of the script
echo '<div class="readout"><p>Total Execution Time <strong>' . round($execution_time) . ' minutes</strong> or ' . round(($time_end - $time_start) / $total_count, 2) . ' seconds per site.</p></div>';

?>
</div>
</body>
</html>