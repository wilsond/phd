<?php
include '_global.php';
include '_config.php';
include '_functions.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Who goes first?</title>
    <link rel="stylesheet" href="css/phd.css" />
</head>
<body class="lock-menu">
<?php menu();?>
<div class="show-results">

<?php

if(isset($_GET['which'])){

breakdown($_GET['which']);
//echo '<p><a href="who-first.php">back</a></p>';
}
else{
    echo '<div class="select-country"><h1>Choose a country</h1><ul class="links">';
    $sql = "SELECT table_name FROM information_schema.tables where table_schema='viewports'";
    if ($result = mysqli_query($con, $sql)) {
        while ($row = mysqli_fetch_array($result)) {
            echo '<li><a href="?which='.$row['table_name'].'">'.$row['table_name'].'</li>';
        }
    }
echo '</ul></div>';
}



?>
</div>
</body>
</html>