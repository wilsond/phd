# to do

## PAPER
* Do second pass and add references
* Add more graphs



## CODE 
* Get PHPfmt working correctly DONE

## database
* Add a date marker when the website has been crawled DONE


## view.php
* Use the date marker so I can figure out what to show DONE

## do-crawl.php
* Add counter for 'testing site'... DONE
* Add timer so I can get an idea of how long it takes to crawl sites DONE
* Add a way of picking up from the last crawled (related to database above) DONE

## debug.php
* create - allow user to add website url
* format so I can understand what's going on: what's returned from the API for a website, what is found for the metadata, etc



