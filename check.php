<?php

include '_config.php';
include '_functions.php';
include '_global.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>API checking for archive.org</title>
    <link rel="stylesheet" type="text/css" href="css/phd.css">

</head>
<body>
<?php menu();?>


<?php

if (!isset($_GET['url'])) {

    echo '<div class="geturl"><form action="check.php" method="get"><label for="url">Enter a URI</label><input type="text" name="url" /><input type="submit" value="check" /></form></div>';

} else {

    $url = $_GET['url'];

    echo '<div class="debug">';

    echo '<h1>Check the system for <strong>' . $url . '</strong></h1>';

    // what comes out of the API for this site?

    echo '<h2 class="check-area">Raw dump of API call</h2>';
    $check_file = 'http://web.archive.org/cdx/search/cdx?url=' . $url;

    $list = @file($check_file);

    if (count($list) > 1) {
        echo '<pre>';
        foreach ($list as $l) {echo $l;}
        echo '</pre>';
    } else {
        echo '<p>This site is not in the internet archive.</p>';exit;
    }

// try removing anything that's not text/html
    $clean_list = array();
    foreach ($list as $l) {
        if (strstr($l, 'warc/revisit')) {array_push($clean_list, $l);}

        if (strstr($l, 'text/html')) {array_push($clean_list, $l);}

    }

    //print_r($clean_list);

    //exit;

    // what do we check?
    $test_urls = array();
    $current_year = '';
    foreach ($clean_list as $l) {
        // comment this back in to see what's coming out from the API
        //echo $l.'<br />';
        $l = explode(' ', $l);

        $test_date = substr($l[1], 0, 4);
        if ($test_date !== $current_year) {
            $test_urls[$test_date] = $l[1];
            $current_year = $test_date;
        }
    }

    echo '<h2 class="check-area">API records to check</h2><ul class="bulleted-list">';

    foreach ($test_urls as $t) {
        echo '<li><a href="http://web.archive.org/web/' . $t . 'id_/' . $url . '">http://web.archive.org/web/' . $t . 'id_/' . $url . '</a></li>';
    }
    echo '</ul>';
    // what comes back from the internet archive?

    echo '<h2 class="check-area">cURL to the website in the internet archive</h2>';

    foreach ($test_urls as $t) {

        $ia_url = 'http://web.archive.org/web/' . $t . 'id_/' . $url;

        echo '<h3>Testing <strong>' . $ia_url . '</strong></h3>';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $ia_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        //curl_setopt($ch, CURLOPT_TIMEOUT, 400);

        $data = curl_exec($ch);

        curl_close($ch);

        $doc = new DOMDocument();
        @$doc->loadHTML($data);

        $metas = $doc->getElementsByTagName('meta');

        $found_meta = '';
        echo '<table>';
        for ($i = 0; $i < $metas->length; $i++) {

            $meta = $metas->item($i);
            echo '<tr><!-- <td>' . $meta->getAttribute('property') . '</td> --><td>' . $meta->getAttribute('name') . '</td><td>' . $meta->getAttribute('content') . '</td></tr>';

// some are doing this twice?

        }
        echo '</table>';
        // end checking
    }
}

?>
    </body>
</html>