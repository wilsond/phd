-- phpMyAdmin SQL Dump
-- version 4.6.5.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 14, 2018 at 12:20 PM
-- Server version: 5.6.34
-- PHP Version: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `viewports`
--

-- --------------------------------------------------------

--
-- Table structure for table `ranking-university`
--

CREATE TABLE `ranking-university` (
  `id` int(11) NOT NULL,
  `site_name` text NOT NULL,
  `site_url` text NOT NULL,
  `initial_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `api_crawls` int(11) NOT NULL,
  `v_2007` text NOT NULL,
  `v_2008` text NOT NULL,
  `v_2009` text NOT NULL,
  `v_2010` text NOT NULL,
  `v_2011` text NOT NULL,
  `v_2012` text NOT NULL,
  `v_2013` text NOT NULL,
  `v_2014` text NOT NULL,
  `v_2015` text NOT NULL,
  `v_2016` text NOT NULL,
  `v_2017` text NOT NULL,
  `last_collected` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ranking-university`
--

INSERT INTO `ranking-university` (`id`, `site_name`, `site_url`, `initial_add`, `api_crawls`, `v_2007`, `v_2008`, `v_2009`, `v_2010`, `v_2011`, `v_2012`, `v_2013`, `v_2014`, `v_2015`, `v_2016`, `v_2017`, `last_collected`) VALUES
(17, 'University of Oxford', 'ox.ac.uk', '2018-08-05 11:22:22', 7339, '', '', '', '', '', '', '', '', 'width=device-width, initial-scale=1.0 maximum-scale=2.0 user-scalable=yes', 'width=device-width, initial-scale=1.0 maximum-scale=2.0 user-scalable=yes', 'width=device-width, initial-scale=1.0 maximum-scale=2.0 user-scalable=yes', '2018-08-05 11:22:22'),
(18, 'University of Cambridge', 'cam.ac.uk', '2018-08-05 11:22:53', 9264, '', '', '', '', '', '', '', 'width=device-width, width=device-width, initial-scale=1.0', 'width=device-width, width=device-width, initial-scale=1.0', 'width=device-width, width=device-width, initial-scale=1.0', 'width=device-width, width=device-width, initial-scale=1.0', '2018-08-05 11:22:53'),
(19, 'California Institute of Technology', 'caltech.edu', '2018-08-05 11:23:27', 7193, '', '', '', '', '', '', 'width=1000, maximum-scale=1.0', 'width=1000, maximum-scale=1.0', 'width=1000, maximum-scale=1.0', 'width=device-width, initial-scale=1', 'width=device-width, initial-scale=1', '2018-08-05 11:23:27'),
(20, 'Stanford University', 'stanford.edu', '2018-08-05 11:24:16', 32371, '', '', '', '', '', 'width=device-width, user-scalable=yes', 'width=device-width, user-scalable=yes', 'width=device-width, initial-scale=1.0', 'width=device-width, initial-scale=1.0', 'width=device-width, initial-scale=1.0', 'width=device-width, initial-scale=1.0', '2018-08-05 11:24:16'),
(21, 'Massachusetts Institute of Technology', 'mit.edu', '2018-08-05 11:24:54', 13697, '', '', '', '', '', 'width=device-width, initial-scale=1.0', 'width=device-width, initial-scale=1.0', 'width=device-width, initial-scale=1.0', 'width=device-width, initial-scale=1.0', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0', '2018-08-05 11:24:54'),
(22, 'Harvard University', 'harvard.edu', '2018-08-05 11:25:33', 12571, '', '', '', '', '', 'width=988', 'width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1', 'width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1', 'width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1', 'width=device-width, minimum-scale=1, initial-scale=1', 'width=device-width, minimum-scale=1, initial-scale=1', '2018-08-05 11:25:33'),
(23, 'Princeton University', 'princeton.edu', '2018-08-05 11:26:23', 13831, '', '', '', '', '', '', '', 'width=device-width, initial-scale=1.0', 'width=device-width, initial-scale=1.0', 'width=device-width, initial-scale=1.0', '', '2018-08-05 11:26:23'),
(24, 'Imperial College London', 'imperial.ac.uk', '2018-08-05 11:26:56', 5495, '', '', '', '', '', '', '', '', 'width=device-width, initial-scale=1.0, maximum-scale=1.3', 'width=device-width, initial-scale=1.0, maximum-scale=1.3', 'width=device-width, initial-scale=1.0, maximum-scale=1.3', '2018-08-05 11:26:56'),
(25, 'University of Chicago', 'uchicago.edu', '2018-08-05 11:27:35', 7308, '', '', '', '', 'width=device-width; maximum-scale=1.0; user-scalable=0;', 'width=device-width, maximum-scale=1, user-scalable=0', 'width=device-width, initial-scale=1, user-scalable=0', 'width=device-width, initial-scale=1, user-scalable=0', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0', '2018-08-05 11:27:35'),
(26, 'ETH Zurich – Swiss Federal Institute of Technology', 'ethz.ch', '2018-08-05 11:28:27', 4079, '', '', '', '', '', '', '', '', '', '', '', '2018-08-05 11:28:27'),
(27, 'University of Pennsylvania', 'upenn.edu', '2018-08-05 11:28:52', 5827, '', '', '', '', '', '', '', '', '', 'width=device-width', 'width=device-width', '2018-08-05 11:28:52'),
(28, 'Yale University', 'yale.edu', '2018-08-05 11:29:22', 8200, '', '', '', '', '', '', 'width=device-width, target-densitydpi=160dpi, initial-scale=1.0', 'width=device-width, target-densitydpi=160dpi, initial-scale=1.0', 'width=device-width, target-densitydpi=160dpi, initial-scale=1.0', 'width=device-width, initial-scale=1', 'width=device-width, initial-scale=1', '2018-08-05 11:29:22'),
(29, 'Johns Hopkins University', 'jhu.edu', '2018-08-05 11:29:50', 6815, '', '', '', '', '', '', '', '', '', 'width=device-width, initial-scale=1', 'width=device-width, initial-scale=1', '2018-08-05 11:29:50'),
(30, 'Columbia University', 'columbia.edu', '2018-08-05 11:30:25', 13287, '', '', '', '', '', 'maximum-scale=1.0, user-scalable=yes', 'maximum-scale=1.0, user-scalable=yes', 'maximum-scale=1.0, user-scalable=yes', 'maximum-scale=1.0, user-scalable=yes', 'maximum-scale=1.0, user-scalable=yes', 'maximum-scale=1.0, user-scalable=yes', '2018-08-05 11:30:25'),
(31, 'University of California, Los Angeles', 'ucla.edu', '2018-08-05 11:30:56', 7726, '', '', '', '', '', '', 'width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0', 'width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0', 'width=device-width, user-scalable=yes, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.0', 'width=device-width, user-scalable=yes, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.0', 'width=device-width, user-scalable=yes, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.0', '2018-08-05 11:30:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ranking-university`
--
ALTER TABLE `ranking-university`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ranking-university`
--
ALTER TABLE `ranking-university`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;