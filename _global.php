<?php

function menu()
{
    echo '<nav>
    <ul>
    <li><a href="index.php">Country List</a></li>
    <li><a href="add-arbitrary.php">Add an arbitrary list to database</a></li>
    <li><a href="check.php">Check a website</a></li>
    <li><a href="view.php">View a graph of results by country</a></li>
    <li><a href="who-first.php">Country breakdown</a></li>
    <li><a href="view-finding.php">View findings graphs by country</a></li>
    <li><a href="media.php">Metatag content analysis</a></li>
    <li><a href="media-graph-csv.php">CSVs of data</a></li>
    </ul>
    </nav>';

}
