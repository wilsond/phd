<?php
include '_global.php';
include '_config.php';
include '_functions.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Media query data</title>
    <link rel="stylesheet" href="css/phd.css" />
</head>
<body>
<?php menu();?>

<ul class="country-list">
<?php

$json = file_get_contents('https://raw.githubusercontent.com/Hipo/university-domains-list/master/world_universities_and_domains.json');
$obj = json_decode($json);
//var_dump($obj);

$all_countries = array();

foreach ($obj as $o) {
    array_push($all_countries, $o->country);
}

$all_countries = array_unique($all_countries);

sort($all_countries);

foreach ($all_countries as $a) {
    echo '<li><p>' . $a . '</p>';

    $c = slug($a);
    $sql = 'SELECT 1 FROM `' . $c . '-university` LIMIT 1';

    $result = mysqli_query($con, $sql);
    if (isset($result->current_field) && $result->current_field !== '') {
        echo '<a class="go" href="do-crawl.php?c=' . urlencode($a) . '">Run initial crawl</a>';
        echo '<a class="go less" href="re-crawl.php?c=' . urlencode($a) . '">Run re-crawl</a>';

// find out how many are empty
        $sql = "SELECT count(*) FROM `" . $c . "-university` WHERE last_collected = '0000-00-00 00:00:00';";

        $result = mysqli_query($con, $sql);
        $row = mysqli_fetch_array($result);

        if ($row[0] == 0) {echo '<p class="crawl-good">All crawled OK</p>';} else {echo '<p class="crawl-bad">' . $row[0] . ' crawls missing</p>';}

    } else {

        echo '<a class="wait"  href="add-country.php?c=' . urlencode($a) . '">Add to database</a>';
    }

    echo '</li>';
}

?>
</ul>
</body>
</html>