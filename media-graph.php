<?php

include '_config.php';
include '_functions.php';
include '_global.php';

$overall_count = 0;
$overall_api = 0;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>viewport tag contents</title>
    <link rel="stylesheet" type="text/css" href="css/phd.css">

</head>
<body>
<?php menu();?>
<div class="graphs">
<h2>Media queries for all countries tested</h2>
<?php

$sql = "SELECT table_name FROM information_schema.tables where table_schema='viewports' order by table_name ASC;";

$check_country = array();
$result = mysqli_query($con, $sql);

if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {

        array_push($check_country, $row['table_name']);

    }
}

$total_sites = 0;

foreach ($check_country as $c) {
    $sql = "SELECT COUNT(`site_name`) AS `uni-count` FROM `$c` ";

    //echo $sql;

    $result = mysqli_query($con, $sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $total_sites = $total_sites + $row['uni-count'];

        }
    }
}

do_analysis_no_table($check_country, $total_sites);
?>
</div>

</body>
</html>
