## Add a new list of urls

* get the urls
* make them name, url
* open in excel, add [type], e.g. institution and location fields
* final CSV should be [type], [location], [name], [url]

## Create the database

* go to the viewport-data database
* run this query

```
CREATE TABLE `[table name]` (
  `id` int(11) NOT NULL,
  `site_name` text NOT NULL,
  `site_url` text NOT NULL,
  `site_type` text NOT NULL,
  `site_location` text NOT NULL,
  `v_2007` text NOT NULL,
  `v_2008` text NOT NULL,
  `v_2009` text NOT NULL,
  `v_2010` text NOT NULL,
  `v_2011` text NOT NULL,
  `v_2012` text NOT NULL,
  `v_2013` text NOT NULL,
  `v_2014` text NOT NULL,
  `v_2015` text NOT NULL,
  `v_2016` text NOT NULL,
  `v_2017` text NOT NULL,
  `v_now` text NOT NULL,
  `last-collected` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

* update id to be auto-increment
* modify _config.php and rename the old one descriptively
* go to http://localhost/phd/crawl/database/ and follow along....
