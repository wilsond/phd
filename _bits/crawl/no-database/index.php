<?php

function get_data($url) {
    
    $ch = curl_init();
	$timeout = 5;
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$data = curl_exec($ch);
    curl_close($ch);
   // echo $data;


$doc = new DOMDocument();
@$doc->loadHTML($data);

$metas = $doc->getElementsByTagName('meta');

for ($i = 0; $i < $metas->length; $i++)
{
$meta = $metas->item($i);
if($meta->getAttribute('name')=="viewport"){
//    return $meta->getAttribute('name'). ' '.$meta->getAttribute('content');

    return $meta->getAttribute('content');
}
}
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Meta tag table</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <style type="text/css">
body {font-family:'roboto condensed'; font-size: 1rem; color: #333; padding: 2rem;}
h1,h2,h3,p {font-size: 1rem; color: #333; font-weight:normal;}
table {border-collapse:collapse; width: 100%;}
th, td {text-align:left; }
th {border-bottom:1px solid #ccc; font-weight:normal; text-align:center; padding: 12px 0;}
td {border-bottom:1px solid #eee; font-size: 0.7rem; padding: 7px 0;}
td+td+td+td {text-align:center;}
td+td+td+td {width:8.33333%;}
td h1 {font-size: 1rem;}
tr.blog td {color: red;}
tr.social-media td {color: green;}
    </style>
</head>
<body>
    <table>
        <tr><th></th>
        <th></th>
        <th></th>
        <th>2007</th>
        <th>2008</th>
        <th>2009</th>
        <th>2010</th>
        <th>2011</th>
        <th>2012</th>
        <th>2013</th>
        <th>2014</th>
        <th>2015</th>
        <th>2016</th>
        <th>2017</th>
</tr>

<?php

//http://archive.org/wayback/available?url=zeldman.com&timestamp=20060101
include 'build-list.php';


$urls = getlist();

// print_r($urls);

// exit;

foreach($urls as $url){

echo '<tr class="'.$url['class_name'].'"><td>'.$url['type'].'</td><td>'.$url['country'].'</td><td><h1>'.$url['url'].'</h1></td>';



for($i=2007; $i<2018; $i++){

$json = file('http://archive.org/wayback/available?url='.$url['url'].'&timestamp='.$i.'0101');

//print_r($json);

// $json = '{"url": "zeldman.com", "timestamp": "20070101", "archived_snapshots": {"closest": {"status": "200", "available": true, "url": "http://web.archive.org/web/20070101033447/http://www.zeldman.com:80/", "timestamp": "20070101033447"}}}';
$get_url = json_decode($json[0], true);

//var_dump($get_url, TRUE);

// $get_code = file();

// $get_code = implode('',$get_code);

// //echo '<textarea>'.$get_code.'</textarea>';

//$get_url['archived_snapshots']['closest']['url']

$returned_content = @get_data($get_url['archived_snapshots']['closest']['url']);

//print_r($meta_options);

echo '<td>'.$returned_content.'</td>';

flush();

}

echo '</ul>';
}
?>
</table>
</body>
</html>