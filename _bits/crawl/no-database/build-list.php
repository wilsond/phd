<?php

function slug($text){
    return str_replace(' ', '-', strtolower($text));
}

function getlist(){

    $listing = array();

if (($handle = fopen("../lists/site-list.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

        $alist['class_name'] = slug($data[0]);
        $alist['type'] = $data[0];
        $alist['country'] = $data[1];
        $alist['url'] = $data[2];
        array_push($listing, $alist);
    
    
    }
}
return $listing;
}
?>