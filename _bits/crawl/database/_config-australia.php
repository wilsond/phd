<?php

// when do we search between?

$start_year = 2007;
$finish_year = 2018;

$csv_name = 'australian-university.csv';
$database_table = 'australian-university';


// allowed distance between archive.org version and time we want to check

//$allowed_distance = 60 * 60 * 24 * 100; // 100 days
$allowed_distance = 60*60*24*30; // one month

// connect to the db

$user = 'root';
$password = 'root';
$db = 'viewport-data';
$host = 'localhost';
$port = 3306;

$con = mysqli_connect( 
   $host, 
   $user, 
   $password, 
   $db,
   $port
);

?>