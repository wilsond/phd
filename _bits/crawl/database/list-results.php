<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crawl Results</title>
    <link rel="stylesheet" type="text/css" href="results.css">

</head>
<body>
<h1>Crawl results</h1>
<p class="inline-list">Choose a table: 
<?php

include '_config.php';
include '_functions.php';

$sql="SELECT table_name FROM information_schema.tables where table_schema='viewport-data';";
$result=mysqli_query($con,$sql);
while ($row=mysqli_fetch_array($result))
{
 echo '<a href="list-results.php?use='.$row[0].'">'.$row[0].'</a>'; 
}
?></p>
<body>
    <table>
        <tr><th></th>
        <th></th>
        <th></th>
        <th>2007</th>
        <th>2008</th>
        <th>2009</th>
        <th>2010</th>
        <th>2011</th>
        <th>2012</th>
        <th>2013</th>
        <th>2014</th>
        <th>2015</th>
        <th>2016</th>
        <th>2017</th>
        <th>now</th>
</tr>


<?php

$sql="SELECT * FROM `".$_GET['use']."`";

if ($result=mysqli_query($con,$sql))
  {
  while ($row=mysqli_fetch_array($result))
    {
      echo '<tr><td>'.$row['site_type'].'</td><td>'.$row['site_location'].'</td><td><h1><a href="'.$row['site_url'].'">'.$row['site_name'].'</a></h1></td>';

    //print_r($row);
    for($i=$start_year; $i<$finish_year; $i++){
      $current_year = 'v_'.$i;
      if($row[$current_year]!==""){echo '<td class="found">&bull;</td>';}else{
      echo '<td>&nbsp;</td>';
    }
    }

    if($row['v_now']!==""){echo '<td class="found">&bull;</td>';}else{
      echo '<td>&nbsp;</td>';
    }

    echo '</tr>';
    }
  mysqli_free_result($result);
}

?>
</table>
</body>
</html>
