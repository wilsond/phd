<?php

function slug($text){
    return str_replace(' ', '-', strtolower($text));
}

function getlist($file){

    $listing = array();

if (($handle = fopen($file, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

        $alist['name'] = $data[2];
        $alist['type'] = $data[0];
        $alist['country'] = $data[1];
        $alist['url'] = $data[3];
        array_push($listing, $alist);
    
    
    }
}
return $listing;
}
?>