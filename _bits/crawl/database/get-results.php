<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crawl Results</title>
    <link rel="stylesheet" type="text/css" href="results.css">

</head>
<body class="graph">

<?php

include '_config.php';
include '_functions.php';

do_graph('uk-university','Viewport code in UK university websites','', 'show-number');

do_graph('australian-university','Viewport code in Australian university websites','', 'show-number');


$rg_ids = array('79','83','84','13','18','93','95','96','27','29','103','106','65','70','43','71','109','50','51','114','116','75','129','134');

do_graph('uk-university','Viewport code in UK Russell Group University websites',$rg_ids, 'show-number');

do_graph('international-blog', 'Viewport code in industry websites', '', 'show-number');

?>

</body>
</html>
