# Russell Group Universities
ID  Name
79  University of Birmingham
83  University of Bristol
84  University of Cambridge
13  Cardiff University
18  Durham University
93  University of Edinburgh
95  University of Exeter
96  University of Glasgow
27  Imperial College London
29  King's College London
103 University of Leeds
106 University of Liverpool
65  London School of Economics and Political Science
70  University of Manchester
43  Newcastle University
71  University of Nottingham
109 University of Oxford
50  Queen Mary University of London
51  Queen’s University Belfast
114 University of Sheffield
116 University of Southampton
75  University College London
129 University of Warwick
134 University of York