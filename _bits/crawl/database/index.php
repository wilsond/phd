<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crawl a set of sites</title>
    <link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
<?php include '_config.php'; ?>
    <h1>Crawl a list of sites for viewport metadata</h1>
    <p>Current config for database table: <strong><?php echo $database_table; ?></strong></p>
    <ul>
    <li><a href="initial-list-population.php">Populate database from CSV</a></li>
    <li><a href="do-crawl.php">Run the crawl using the list</a></li>
    <li><a href="list-results.php">Show results of the crawl</a></li>
    <li><a href="get-results.php">Graph the crawl results</a></li>
    </ul>

    <ul><li><a href="new-api.php">Check what's coming back from the API</a></li></ul>
</body>
</html>