
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>API checking for archive.org</title>
    <style type="text/css">
    body {font-family:'work sans'; font-size:1em; max-width: 1000px; margin: 2em auto;}
    h2 {border-top:1px solid #777;}
    p {margin:0; padding: 0 0 0.5em 0;}
    </style>
</head>
<body>
    


<?php
include('_functions.php');
$start_year = 2007;
$finish_year = 2019;
$url="port.ac.uk";
//$url="anglia.ac.uk";
//$url="mmu.ac.uk";
$allowed_distance = 60*60*24*30; // 1 month
$id = 'test';
$database_table = "test";

echo '<h1>Get results from archive.org for '.$url.'</h1>';

    for($i=$start_year; $i<$finish_year; $i++){
     echo '<h2>'.$i.'</h2>';

     // build the URL for our request from the archive

// don't edit this to put &amp; in the string!!!     
    $archive_url = 'http://archive.org/wayback/available?url='.$url.'&timestamp='.$i.'0101';

    echo str_replace('&','&amp;',$archive_url);
    
    // do the curl request

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	curl_setopt($ch, CURLOPT_URL, $archive_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
    $json = curl_exec($ch);
    curl_close($ch);

    // decode the json

    $get_url = json_decode($json, true);

    echo '<pre>';
    print_r($get_url);
    echo '</pre>';

    // get the year, month, day of the request

    $test_returned = @substr($get_url['archived_snapshots']['closest']['timestamp'],0,8);

    echo '<p>The time of the closest item in the archive: '.$test_returned.' &middot; Unix '.strtotime($test_returned).'</p>';

    // these tests are wrong
    // return three things
    // 1. the viewport meta tag value
    // 2. we didn't get a return within a sensible timespan from the archive.org site
    // 3. we didn't get any data from the archive.org site


    $code = "";
    $test_age = $i.'0101';

    echo '<p>The time we are testing: '.$test_age.' &middot; Unix '.strtotime($test_age).'</p>';



    $distance = abs(strtotime($test_returned) - strtotime($test_age));

    echo '<p>Distance in seconds: '.$distance.'</p>';

    echo '<p>Distance in human readable form: '.floor($distance / (24*60*60)/365).' years</p>';

if($distance < $allowed_distance ){
 
    $code = get_data($get_url['archived_snapshots']['closest']['url']);  

} else {
    //$code = "no other data";
    $code = "";
}



$sql = "UPDATE `$database_table` SET `v_".$i."` = '".$code."' WHERE `$database_table`.`id` = ".$id.";";
echo "<p>SQL query: ".$sql.'</p>';
   //$result=mysqli_query($con,$sql);
   flush();


    }
    

    ?>
    </body>
</html>