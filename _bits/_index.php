<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Top websites viewport tag</title>
    <style type="text/css">
    body {margin: 1em; font-family:sans-serif; color: #333; }
    ul {margin:0;padding:0; list-style-type:none;}
    ul li {padding: 10px; border-bottom:1px solid #ddd;}
    ul li span {display:inline-block; width: 25%;}
    ul li span+span {color: red; text-align:center;}
</style>
</head>
<body>
<ul>


<?php


$list = file('test-data-long.txt');
$list=implode('', $list);
$list = explode('<tr>',$list);
$total = 0;
$mention = 0;
$hit = 0;
foreach($list as $l){
$total=$total+1;
    $m = explode('</td>',$l);
    $m[1]=str_replace('<td>', '', $m[1]);
 if($m[1]!==""){
 $check = file_get_contents('http://'.trim($m[1]));
 // find a metatag
echo '<li><span class="name">'.$m[1].'</span>';
 if(strstr($check,'viewport')){echo '<span class="mention">viewport mentioned</span>';$mention=$mention+1;}else {echo '<span class="mention">&#215;</span>'; }

 if(strstr($check,'meta name="viewport"')){echo '<span class="mention">viewport tag found</span>';$hit=$hit+1;}else {echo '<span class="mention">&#215;</span>'; }


 flush();
}


 

}

?>
</ul>
<p>Complete. Total: <?php echo $total; ?>. Mentions: <?php echo $mention; ?>. Hits: <?php echo $hit; ?></p>
</body>
</html>